<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1">
	<meta name="msapplication-TileColor" content="#00ff7f">
	<meta name="theme-color" content="#00ff7f">
	<title>Best Lap</title>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#00ff7f">
	<link rel="shortcut icon" href="/favicon.ico">
	<script src="/socket.io/socket.io.js"></script>
	<script>
	   	var socket = io.connect('https://bestlap.herokuapp.com/', { secure: true });

		var race = <?php include("race.json"); ?>;
		var racers = <?php include("racers.json"); ?>;
		var cars = <?php include("cars.json"); ?>;
		var classes = <?php include("classes.json") ?>;
		var laps = <?php include("laps.json"); ?>;
		var rounds = <?php include("rounds.json"); ?>;
		var points = <?php include("points.json"); ?>;
		var messages = <?php include("messages.json"); ?>;
		var pager = new Pager(messages);
		var index = new Index(racers, cars, laps);
		var timetable = document.createElement('table');
		var scoreboard = document.createElement('table');
		
		var columns = ["number", "driver", "mark", "volume", "turbo", "time"];
		var lap = 3;
		var round = 3;
		var fakenews = null; //setInterval(randomize, 3000);
		
		document.addEventListener('DOMContentLoaded', function () {
			var announcement = document.querySelector('#announcement');
			var bestlap = document.querySelector('#bestlap');

			document.body.appendChild(pager);
			document.body.appendChild(scoreboard);

			draw();
			sort();
			place();
			sortPlaces(scoreboard);
			animate();

			pointify();

			announcement.classList.add('-new');

			pager.addEventListener('message', function () {
				announcement.classList.add('-new');
			})

			announcement.addEventListener('click', function (event) {
				pager.toggle();
				announcement.classList.remove('-new');
			})

			bestlap.addEventListener('click', function (event) {
				window.location.href = 'https://bestlap.ee/';
			})
		});
		
	    socket.on('connection', function () {
	        console.log("socket connect")
	    });
		
	    socket.on('record', function (record) {
			if (typeof record === 'string') {
				record = JSON.parse(record);
			}
			update(record);
			animate();
		});
		
		socket.on('message', function (message, id) {
			if (id === socket.id) {
                return;
			}
			if (typeof message === 'string') {
				message = JSON.parse(message);
			}
			pager.post(message);
			
			new Badge(message.text);
	    });

		window.addEventListener('orientationchange', function () {
			m.style.bottom = 0;
		});
		
		function update (record) {
			scoreboard.setAttribute('data-round', record.round);
			scoreboard.setAttribute('data-lap', record.lap);

			Object.keys(record.numbers).forEach(function (number) {
				if (!index[number]) {
					return;
				}
				var racers = document.querySelectorAll('.racer[data-number="' + number + '"]');
				
				if (!rounds) {
					rounds = [];
				}
				if (typeof rounds[record.round-1] === 'undefined') {
					rounds[record.round-1] = {};
				}
				if (typeof rounds[record.round-1][number] === 'undefined') {
					rounds[record.round-1][number] = "59:59,99";
				}
				if (typeof laps[record.round-1] === 'undefined') {
					laps[record.round-1] = [];
				}
				if (typeof laps[record.round-1][record.lap-1] === 'undefined') {
					laps[record.round-1][record.lap-1] = {};
				}
				if (typeof laps[record.round-1][record.lap-1][number] === 'undefined') {
					laps[record.round-1][record.lap-1][number] = "59:59,99";
				}
				if (record.lap === 'bestlap' || milliseconds(record.numbers[number]) < milliseconds(rounds[record.round-1][number])) {
					rounds[record.round-1][number] = record.numbers[number];
				}
				if (typeof record.numbers[number] === 'number' && record.numbers[number] !== milliseconds(laps[record.round-1][record.lap-1][number])) {
					laps[record.round-1][record.lap-1][number] = format(record.numbers[number]);
				}
				if (typeof record.numbers[number] === 'string' && milliseconds(record.numbers[number]) !== milliseconds(laps[record.round-1][record.lap-1][number]) && record.numbers[number] !== "--:--:--") {
					laps[record.round-1][record.lap-1][number] = record.numbers[number];
				}

				Array.prototype.forEach.call(racers, function (racer) {
					if (racer === null) {
						return;
					}
					var scorecell = racer.querySelector('.round[data-number="' + record.round + '"]');

					if ('addLap' in racer) {
						racer.addLap(record.round, record.lap, laps[record.round-1][record.lap-1][number]);
					}

					if (scorecell.innerText === rounds[record.round-1][number]) {
						return;
					}
					
					scorecell.innerText = rounds[record.round-1][number];
					scorecell.setAttribute('data-lap', record.lap);

					scorecell.classList.add('shift');
					setTimeout(function () {
						scorecell.classList.remove('shift');
					}, 1000);
					setTimeout(animationend, 10);

					compare(record.round-1)

					function animationend () {
						racer.querySelector('.round[data-number="' + record.round + '"]').innerText = rounds[record.round-1][number];
					}
				});
				
			});
			
			sort();
			place();

		}
		
		function draw () {
			scoreboard.innerHTML = '';
			
			Object.keys(classes).forEach(function (classen) {
				if (classes[classen].length === 0) {
					return;
				}
				
				var thead = document.createElement('thead');
				var tbody = document.createElement('tbody');
				var tr = document.createElement('tr');
				var th = document.createElement('th');

				th.innerText = classen;
				th.colSpan = columns.length + 4;
				thead.classList.add("class");
				thead.setAttribute('data-class', classen);
				tbody.classList.add("class");
				tbody.setAttribute('data-class', classen);
				
				thead.appendChild(th);
				scoreboard.append(thead);
				scoreboard.append(tbody);
				
				classes[classen].forEach(function (number) {
					if (!index[number]) {
						return;
					}
					
					var tr = document.createElement('tr');
					var columns = ["number", "driver", "mark", "model", "volume", "turbo", "round-1", "round-2", "round-3"];
					
					
					columns.forEach(function (column) {
						var td = document.createElement('td');

						fill(number, column, td, tr);
						
					});

					
					tr.classList.add("racer");
					tr.setAttribute('data-number', number);


					tr.addEventListener('click', function (event) {
						var open = true;
						var racer = new Racer(number);
						
						columns.forEach(function (column) {
							var div = document.createElement('div');

							fill(number, column, div, racer);
						});
						
						racer.createLaps(rounds, laps);
						racer.classList.add("racer");
						racer.setAttribute('data-number', number);

						try {
							var popup = document.querySelector('.popup');
							if (popup.from === tr) {
								popup.dissolve();
								open = false;
							};
						} catch (error) {
							//console.error('PopUp not exist', error);
						}
						if (open) {
							new PopUp(tr, racer, false, index[number].car.color);
						}
						
						place();
					});
					
					tbody.append(tr);
				});
			});
			
			scoreboard.dispatchEvent(new CustomEvent('complete'));
		}

		function fill (number, column, element, target) {
			if (column.indexOf("round-") >= 0) {
				element.classList.add("round");
				element.setAttribute('data-number', column.split("round-")[1]);
				
			} else {
				element.classList.add(column);
			}
			if (column === "mark") {
				var img = document.createElement('img');

				img.src = '/logos/' + mark(index[number].car.mark) + '.png';

				element.appendChild(img);
			} else {
				element.innerText = select(index, column, number);
			}
			target.appendChild(element);
		}

		function mark (name) {
			var mark = name.toLowerCase().replace(' ', '-');

			switch (mark) {
				case 'vw':
					mark = 'volkswagen';
					break;
			}

			return mark;
		}
		
		
		function select (index, column, number) {
			return column === "number" ? number :
			column === "driver" ? index[number].driver :
			column === "mark" ? index[number].car.mark :
			column === "model" ? index[number].car.model :
			column === "turbo" ? (index[number].car.engine.turbo ? "T" : ""):
			column === "volume" ? (index[number].car.engine.volume / 1000).toFixed(1) :
			column === "time" ? (record.numbers[number] ? record.numbers[number] : '--:--,--') :
			column.indexOf("round-") >= 0 ? (rounds[column.split("round-")[1]-1] && rounds[column.split("round-")[1]-1][number] ? rounds[column.split("round-")[1]-1][number] : '--:--,--') :
			index[number][column];
		}	
		
		function sort () {
			for (var classen in classes) {
				if (classes[classen].length === 1) {
					compare(classes[classen][0]);
					continue;
				}
				classes[classen].sort(function (a, b) {
					var compared = compare(a, b);
					
					return compared[0] - compared[1];
				});
			}
		}

		function sortPlaces (scoreboard) {
			var classtbodies = scoreboard.querySelectorAll('tbody');

			Array.prototype.forEach.call(classtbodies, function (classtbody) {
				var classen = classtbody.dataset.class;

				sortTable(classtbody);
			});
		}

		function sortTable (table) {
			console.log('tttt')
			var shouldSwitch;
			var switching = true;

			while (switching) {
				switching = false;
				var rows = table.rows;

				for (var i = 0; i < (rows.length - 1); i++) {
					shouldSwitch = false;

					if (rows[i].dataset.place > rows[i + 1].dataset.place) {
						shouldSwitch = true;
						break;
					}
				}
				if (shouldSwitch) {
					rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
					switching = true;
				}
			}
		}

		function Pager (messages) {
			var Pager = document.createElement('div');
			var open = false;

			if (messages instanceof Array) {
				messages.forEach(function (message) {
					place(message);
				});
			}

			Pager.open = function () {
				if (open) {
					return;
				}
				Pager.classList.add('-open');
				Pager.scrollTop = Pager.scrollHeight;
				open = true;
			}

			Pager.close = function () {
				if (!open) {
					return;
				}
				Pager.classList.remove('-open');
				open = false;
			}

			Pager.toggle = function () {
				if (open) {
					Pager.close();
				} else {
					Pager.open();
				}
			}

			Pager.post = function (message) {
				place(message);

				Pager.dispatchEvent(new CustomEvent('message',  {
					bubbles: true,
					cancelable: true,
					detail: {
						message: message
					}
				}));
			}

			Pager.classList.add('pager');

			function place (message) {
				var messageDiv = document.createElement('div');
				var date = new Date(message.time)

				messageDiv.classList.add('message')
				messageDiv.innerText = message.text;
				messageDiv.dataset.time = date.getHours() + ':' + date.getMinutes();

				Pager.appendChild(messageDiv)
				Pager.scrollTop = Pager.scrollHeight;

				Pager.classList.add('-new');
				setTimeout(function () {
					Pager.classList.remove('-new');
				}, 3000)
			}

			return Pager;
		}

		function Badge (text) {
			var Badge = document.createElement('div');
			var SHOWTIME = 5000;
			var HIDETIME = 1000;

			Badge.innerText = text;
			Badge.classList.add('badge');

			document.body.appendChild(Badge);

			setTimeout(show, HIDETIME);
			
			function show () {
				Badge.classList.add('-show');
				setTimeout(dissolve, SHOWTIME);
			}

			function dissolve () {
				Badge.classList.remove('-show');
				Badge.classList.add('-removing');
				setTimeout(remove, HIDETIME);
			}

			function remove () {
				document.body.removeChild(Badge);
			}

			return Badge;
		}
	
		/**
		* Compares two racers' rounds' best lap times and highlights it on the scoreboard.
		* @param {Number} a Racer A
		* @param {Number} b Racer B
		* @return {Array}
		*/
		function compare (a, b) {
			var x = Infinity;
			var y = Infinity;
			
			rounds.forEach(function (round, index) {
				if (typeof round[a] === 'undefined') {
					return;
				}
				if (milliseconds(round[a]) > 0 && milliseconds(round[a]) < x) {
					x = milliseconds(round[a]);
					highlight(a, index+1, round[a]);
				}
				if (typeof b === 'undefined' || typeof round[b] === 'undefined') {
					return;
				}
				if (milliseconds(round[b]) > 0 && milliseconds(round[b]) < y) {
					y = milliseconds(round[b]);
					highlight(b, index+1, round[b]);
				}
			});
			
			return [x, y];
		}
		
		function highlight (number, round, time) {
			var racers = scoreboard.querySelectorAll('.racer[data-number="' + number + '"]');

			Array.prototype.forEach.call(racers, function (racer) {
				if (racer === null) {
					scoreboard.addEventListener('complete', function () {
						highlight(number, round, time);
					});
					return;
				}
				Array.prototype.forEach.call(racer.querySelectorAll('.round'), function (roundcell) {
					roundcell.classList.remove('bestlap');
					roundcell.setAttribute('data-bestlap', time);
				})
				racer.querySelector('.round[data-number="' + round + '"]').classList.add('bestlap');

				if ('bestlap' in racer) {
					racer.bestlap();
				}
			});
			
		}

		function place () {
			for (var classen in points) {
				if (classes[classen].length <= 0) {
					continue;
				}
				points[classen].forEach(function (racer, index) {
					var number = racer[0];
					var pts = racer[1];
					var place = index + 1;

					var racercells = document.querySelectorAll('.racer[data-number="' + number + '"]');

					Array.prototype.forEach.call(racercells, function (racercell) {
						var place = index + 1;
						racercell.setAttribute('data-place', place);
						racercell.setAttribute('data-points', pts);
					});
				});
			}
		}
		
		function milliseconds (formatted, hours) {
			if (formatted === String()) {
				return 0;
			}
			var time = formatted.split(':');
			var hour = hours ? time[0] * 60 * 60 * 1000 : 0;
			var minute = (hours ? time[1] : time[0]) * 60 * 1000;
			var second = (hours ? time[2] : time[1]).split(',');
			var roundsecond = second[0] * 1000
			var millisecond = parseInt(pad(second[1], 3, true));
			
			return hour + minute + roundsecond + millisecond;
		}
		
		
		function format (milliseconds, hours) {
			if (!(parseInt(milliseconds) >= 0)) {
				return '--:--,--';
			}
			var remainer = milliseconds;
			var millisecond = remainer % 1000;
			remainer = (remainer - millisecond) / 1000;
			var roundsecond = remainer % 60;
			remainer = (remainer - roundsecond) / 60;
			var minute = remainer % 60;
			var hour = (remainer - minute) / 60;

			var countMillisecond = pad(Math.round(millisecond / 10));

			if (countMillisecond === '100') {
				roundsecond = roundsecond + 1;
				countMillisecond = '00';
			} 

			return (hours ? pad(hour) + ":" : "") + pad(minute) + ':' + pad(roundsecond) + ',' + countMillisecond;
		}
		
		function pad (number, size, right) {
			var result = number.toString();
			while (result.length < (size || 2)) result = (right ? "": "0") + result + (right ? "0": "");
			return result.substring(0, right ? (size || 2) : number.toString().lenght);
		}
		
		function animate () {
			var tables = document.querySelectorAll('table');
			
			Array.prototype.forEach.call(tables, function (table) {
				var classbodies = table.querySelectorAll('tbody[data-class]');
				
				Array.prototype.forEach.call(classbodies, function (classbody, index) {
					var classen = classbody.dataset.class;
					var racerrows = classbody.querySelectorAll('tr[data-place]');
					var racerrow = classbody.querySelector('tr[data-place]');
					if (racerrow === null) {
						return;
					}
					var height = racerrow.offsetHeight;
					
					classbody.style.height = height * classes[classen].length;
					
					Array.prototype.forEach.call(racerrows, function (racerrow, index) {
						racerrow.style.position = 'absolute';
						racerrow.style.order = racerrow.dataset.place + 1;
						racerrow.style.top = (height * (racerrow.dataset.place - 1)) + "px";
						
					});
				});
			});
		}
		
		function afterreorder () {
			
		}
		
		function fakestart () {
			return fakenews = setInterval(randomize, 1000);
		}
		
		function fakestop () {
			clearInterval(fakenews);
		}
		
		var classen = JSON.parse(JSON.stringify(classes['Street D']));
		

		function randomize () {
			if (round === 3 && lap === 3) {
				stop();
				return;
			}
			if (round === 0) {
				round = 1;
				lap = 1;
			}
			if (lap === 3) {
				round = round + 1;
				lap = 1;
				classen = JSON.parse(JSON.stringify(classes['Street D']));
			}
			if (classen.length === 0) {
				classen = JSON.parse(JSON.stringify(classes['Street D']));
				lap = lap + 1;
			}

			var record = {};
			var number = random(classen.length);
			var racer = classen[number];
			var time = format(120000 + random(10000));

			record.round = round;
			record.lap = lap;
			record.numbers = {};
			record.numbers[racer] = time;

			classen.splice(number, 1); 
			
			update(record);
			animate();
		}
		
		function start () {
			if (lap === 3) {
				lap = 0;
			}
			if (round === 3) {
				round = 0;
			}

			fakestart();
		}
		
		function stop () {
			fakestop();
			pointify();
			scoreboard.removeAttribute('data-round');
			scoreboard.removeAttribute('data-lap');
			console.log('%c  simulation complete  ', 'color: black; background: springgreen');
		}
		
		function random (max) {
		  return Math.floor(Math.random() * Math.floor(max));
		}
		
		function pointify () {
			for (classen in classes) {
				var last = classen.length;
				var current = last;
				var loop = setInterval(interval, 100);
				
				function interval () {
					if (classen.length === 0 || current === 0) {
						clearInterval(loop);
						return;
					}
					var number = random(classen.length);
					var racerrows = document.querySelectorAll('tr[data-place="' + current +'"]');
					
					Array.prototype.forEach.call(racerrows, function (racerrow) {
						racerrow.classList.add('points');
					});
					current--;
				}
			};
		}

		function Index (racers, cars, results) {
			var Index = [];

			cars.forEach(function (car) {
				car.number.forEach(function (reg) {
					if (!racers[reg]) return;
					Index[reg] = {
						driver: racers[reg],
						car: car
					};
				});
			})

			return Index;
		}

		function Racer (number) {
			var Racer = getElement('Racer');

			Object.defineProperty(Racer, 'name', {
                get: function() {
                    return name;
                },
                enumerable: true,
                configurable: true
			});

			Racer.createLaps = function (rounds, laps) {
				var lapsElement = createDiv('laps');

				Racer.appendChild(lapsElement);

				laps.forEach(function (round, roundIndex) {
					var roundElement = createDiv('-round');

					roundElement.dataset.number = roundIndex + 1;

					lapsElement.appendChild(roundElement);

					round.forEach(function (lap, lapIndex) {
						if (!(number in lap)) {
							return;
						}
						var lapElement = createDiv('lap');
						var record = laps[roundIndex][lapIndex][number];

						Racer.addLap(roundIndex + 1, lapIndex + 1, record)
					});
					
				});
			};

			Racer.addLap = function (round, lap, record) {
				var exist = true;
				var roundElement = Racer.querySelector('.-round[data-number="' + round + '"]');
				if (!(roundElement instanceof HTMLElement)) {
					exist = false;
					roundElement = createDiv('-round');
					Racer.appendChild(roundElement);
				}
				var lapElement = roundElement.querySelector('.lap[data-lap="' + lap + '"]');
				
				if (!(lapElement instanceof HTMLElement)) {
					exist = false;
					lapElement  = createDiv('lap');
				}

				lapElement.innerText = record;
				lapElement.dataset.round = round;
				lapElement.dataset.lap = lap;

				if (!exist) {
					roundElement.appendChild(lapElement);
				}
			};

			function getElement (name) {
				if (!(Racer instanceof HTMLElement)) {
					return createDiv('Racer');
				}
				var element = Racer.querySelector('.' + name);

				if (element instanceof HTMLElement) {
					return element;
				}

				return createDiv(name);
			}

			function createDiv (name) {
				var div = document.createElement('div')

				div.classList.add(name);

				return div;
			}


			return Racer;
		}

		function PopUp (from, bubble, position, color) {
			var PopUp = document.createElement('div');
			var close = document.createElement('ins');
            var caller = from.getBoundingClientRect();
            var closed = true;

            PopUp.from = from;
            PopUp.dissolve = function () {
                document.body.removeEventListener('click', click);
				PopUp.classList.remove('-presented');
				setTimeout(function () {
					PopUp.parentNode.removeChild(PopUp);
				}, 500)
            }

            PopUp.classList.add('popup');
			PopUp.appendChild(bubble);
			PopUp.appendChild(close);
			if (!!position) {
				PopUp.style.top = caller.top + from.offsetWidth - 10 + 'px';
            	PopUp.style.left = caller.left + from.offsetWidth - 10 + 'px';
			}
			if (!!color) {
				PopUp.classList.add('-' + color);
			}
            document.body.appendChild(PopUp);
			document.body.addEventListener('click', click);
			close.addEventListener('click', function (event) {
				PopUp.dissolve();
			});
			
			setTimeout(function () {
				PopUp.classList.add('-presented');
			}, 10)

            function click (event) {
                if (!closed && !PopUp.contains(event.target)) {
                    PopUp.dissolve()
                }
                closed = false;
            }

            return PopUp;
		}
	</script>
	<link rel="stylesheet" href="../style.css" />
</head>
<body>
<header>
<svg version="1.1" id="bestlap" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
<g>
	<path fill="#C9C9CA" d="M50.001,0C22.39,0,0.002,22.386,0,50c0.002,27.615,22.39,50,50.001,50
		c27.616,0,50.001-22.385,49.999-49.997C100.002,22.383,77.62,0,50.001,0z"/>
	<path fill="#47B549" d="M89.62,50.003c0.001,21.89-17.746,39.64-39.641,39.64C28.082,89.643,10.333,71.89,10.333,50
		c0-21.897,17.747-39.645,39.642-39.643C71.872,10.355,89.62,28.107,89.62,50.003z"/>
	<path fill="#FFFFFF" d="M20.186,32.537h7.043c1.73,0,3.094,0.479,3.958,1.348c0.697,0.692,1.043,1.536,1.043,2.623
		c0,1.709-0.911,2.662-1.996,3.271c1.759,0.666,2.839,1.686,2.839,3.768c0,2.772-2.253,4.158-5.674,4.158h-7.213V32.537z
		 M26.51,38.669c1.474,0,2.403-0.478,2.403-1.647c0-0.996-0.78-1.561-2.188-1.561h-3.291v3.208H26.51z M27.398,44.776
		c1.471,0,2.359-0.521,2.359-1.69c0-1.015-0.755-1.644-2.467-1.644h-3.856v3.334H27.398z"/>
	<path fill="#FFFFFF" d="M37.296,32.537H48.73v2.97h-8.125v3.076h7.151v2.969h-7.151v3.183h8.234v2.969H37.296V32.537z"/>
	<path fill="#FFFFFF" d="M52.241,45.495l1.968-2.365c1.366,1.126,2.796,1.842,4.527,1.842c1.364,0,2.19-0.541,2.19-1.471
		c0-0.844-0.52-1.277-3.055-1.93c-3.055-0.783-5.028-1.622-5.028-4.675c0-2.751,2.212-4.57,5.312-4.57
		c2.207,0,4.092,0.69,5.631,1.921l-1.734,2.516c-1.343-0.932-2.664-1.497-3.943-1.497c-1.278,0-1.947,0.585-1.947,1.366
		c0,1,0.648,1.32,3.271,1.994c3.072,0.798,4.81,1.905,4.81,4.592c0,3.014-2.299,4.698-5.568,4.698
		C56.374,47.916,54.06,47.115,52.241,45.495z"/>
	<path fill="#FFFFFF" d="M71.817,35.614h-4.612v-3.077H79.77v3.077h-4.615v12.09h-3.338V35.614z"/>
	<path fill="#FFFFFF" d="M27.291,53.195h3.334v12.132h7.557v3.032H27.291V53.195z"/>
	<path fill="#FFFFFF" d="M47.366,53.086h3.077l6.497,15.272h-3.488l-1.384-3.407h-6.414l-1.387,3.407H40.87L47.366,53.086z
		 M50.877,62.009l-2.015-4.912l-2.017,4.912H50.877z"/>
	<path fill="#FFFFFF" d="M60.749,53.195h6.199c3.615,0,5.803,2.146,5.803,5.287c0,3.509-2.729,5.326-6.133,5.326h-2.533v4.551
		h-3.336V53.195z M66.729,60.837c1.666,0,2.642-0.989,2.642-2.336c0-1.49-1.04-2.294-2.707-2.294h-2.579v4.63H66.729z"/>
	<path fill="#FFFFFF" d="M89.132,22.978c-0.592,0.599-1.187,1.198-1.781,1.801c-0.563-0.775-1.149-1.537-1.751-2.279
		c0.597-0.602,1.189-1.202,1.786-1.802C87.989,21.441,88.572,22.203,89.132,22.978z"/>
	<path fill="#FFFFFF" d="M85.6,22.5c-0.596,0.597-1.188,1.199-1.783,1.799c-0.607-0.746-1.232-1.477-1.874-2.185
		c0.599-0.599,1.196-1.2,1.794-1.798C84.377,21.029,84.999,21.757,85.6,22.5z"/>
	<path fill="#FFFFFF" d="M81.943,22.114c-0.595,0.596-1.187,1.194-1.784,1.793c-0.646-0.712-1.306-1.408-1.983-2.087
		c0.597-0.595,1.193-1.197,1.792-1.793C80.644,20.705,81.305,21.398,81.943,22.114z"/>
	<path fill="#FFFFFF" d="M85.565,26.582c-0.592,0.603-1.183,1.206-1.779,1.806c-0.566-0.78-1.152-1.542-1.756-2.293
		c0.596-0.597,1.189-1.195,1.787-1.795C84.417,25.046,85.003,25.804,85.565,26.582z"/>
	<path fill="#FFFFFF" d="M85.528,18.516c-0.6,0.602-1.193,1.201-1.791,1.8c-0.639-0.712-1.303-1.409-1.98-2.087
		c0.602-0.594,1.199-1.189,1.799-1.786C84.229,17.117,84.884,17.81,85.528,18.516z"/>
	<path fill="#FFFFFF" d="M81.776,18.231c-0.596,0.602-1.195,1.2-1.789,1.798c-0.679-0.68-1.375-1.338-2.088-1.981
		c0.604-0.599,1.201-1.191,1.799-1.79C80.409,16.899,81.099,17.561,81.776,18.231z"/>
	<path fill="#FFFFFF" d="M77.866,18.047c-0.601,0.595-1.197,1.192-1.795,1.788c-0.715-0.644-1.447-1.269-2.197-1.87
		c0.604-0.596,1.207-1.188,1.805-1.79C76.427,16.783,77.152,17.403,77.866,18.047z"/>
	<path fill="#FFFFFF" d="M81.471,14.436c-0.599,0.594-1.197,1.191-1.795,1.787c-0.711-0.641-1.439-1.264-2.182-1.863
		c0.599-0.595,1.201-1.189,1.801-1.783C80.037,13.179,80.764,13.799,81.471,14.436z"/>
	<path fill="#FFFFFF" d="M77.495,14.359c-0.602,0.593-1.205,1.187-1.802,1.786c-0.743-0.605-1.508-1.19-2.286-1.752
		c0.604-0.592,1.207-1.191,1.806-1.783C75.99,13.175,76.751,13.758,77.495,14.359z"/>
	<path fill="#FFFFFF" d="M79.978,24.087c0.628,0.728,1.233,1.48,1.81,2.252c0.078-0.081,0.16-0.163,0.242-0.245
		c-0.605-0.743-1.23-1.475-1.871-2.187C80.097,23.967,80.039,24.027,79.978,24.087z"/>
	<path fill="#FFFFFF" d="M78.022,21.982c0.055-0.054,0.107-0.107,0.161-0.161c-0.677-0.678-1.373-1.343-2.087-1.983
		c-0.061,0.06-0.121,0.121-0.184,0.181C76.641,20.648,77.342,21.302,78.022,21.982z"/>
	<path fill="#FFFFFF" d="M73.654,18.203c0.079-0.08,0.161-0.158,0.241-0.238c-0.746-0.609-1.51-1.188-2.286-1.757
		c-0.116,0.114-0.231,0.229-0.347,0.344C72.08,17.073,72.877,17.624,73.654,18.203z"/>
	<path fill="#FFFFFF" d="M81.083,10.834c-0.746-0.595-1.512-1.168-2.294-1.72c-0.589,0.58-1.179,1.16-1.767,1.738
		c0.773,0.567,1.531,1.152,2.277,1.752C79.89,12.015,80.487,11.426,81.083,10.834z"/>
	<path fill="#FFFFFF" d="M83.254,12.662c-0.594,0.591-1.188,1.179-1.783,1.774c0.712,0.64,1.405,1.293,2.082,1.969
		c0.594-0.594,1.188-1.186,1.783-1.782C84.661,13.95,83.966,13.296,83.254,12.662z"/>
	<path fill="#FFFFFF" d="M89.16,18.908c-0.379-0.475-0.765-0.942-1.159-1.404c-0.222-0.256-0.443-0.513-0.67-0.766
		c-0.006-0.005-0.011-0.011-0.017-0.018c-0.594,0.602-1.193,1.199-1.786,1.796c0.636,0.712,1.255,1.439,1.858,2.182
		C87.979,20.103,88.568,19.504,89.16,18.908z"/>
</g>
</svg>
</header>
<nav>
	<ul>
		<li><a href="https://results.bestlap.ee/" data-content="results">Tulemused</a></li>
		<!--
		<li><a href="https://bestlap.ee/#regulations" data-content="regulations">Reglement</a></li>
		-->
	</ul>
</nav>
<svg id="announcement" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 528 528">
<circle cy="264" cx="264" r="263.5"/>
<g fill="#fff">
<rect x="113.5" y="226.5" width="12.2" height="83" rx="1"/>
<path d="m125.8 238.4l270.2-58.9v177l-270.2-58.9"/>
</g>
<rect x="184.6" y="262.6" width="105.6" ry="11" rx="13" fill="none" stroke-width="24" stroke="#fff" height="105.6"/>
</svg>

</body>
</html>