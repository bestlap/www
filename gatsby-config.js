const siteMetadata = require('./src/content/meta');

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata,
  plugins: [
    "gatsby-plugin-sass",
    {
      resolve: 'gatsby-plugin-i18n',
      options: {        
        langKeyDefault: 'et',
        useLangKeyLayout: false,
        prefixDefault: false
      }
    },
    {
      resolve: '@mobile-reality/gatsby-plugin-gdpr-cookies',
      options: {
        googleAnalytics: {
          trackingId: '', // leave empty if you want to disable the tracker
          cookieName: 'gatsby-gdpr-google-analytics', // default gatsby-gdpr-google-analytics
          anonymize: true, // default
          allowAdFeatures: false // default
        },
        googleTagManager: {
          trackingId: process.env.GOOGLE || '', // leave empty if you want to disable the tracker
          cookieName: 'consent', // default gatsby-gdpr-google-tagmanager
          dataLayerName: 'dataLayer', // default
        },
        facebookPixel: {
          pixelId: '', // leave empty if you want to disable the tracker
          cookieName: 'gatsby-gdpr-facebook-pixel', // default
        },
        tikTokPixel: {
          pixelId: '', // leave empty if you want to disable the tracker
          cookieName: 'gatsby-gdpr-tiktok-pixel', // default
        },
        hotjar: {
          hjid: '',
          hjsv: '', // YOUR_HOTJAR_SNIPPET_VERSION
          cookieName: 'gatsby-gdpr-hotjar', // default
        },
        linkedin: {
          trackingId: '', // leave empty if you want to disable the tracker
          cookieName: 'gatsby-gdpr-linked-in', // default
        },
        // defines the environments where the tracking should be available  - default is ["production"]
        environments: ['production', 'development']
      },
    },
  ],
  trailingSlash: "always",
};
