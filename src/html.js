import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      <meta name="apple-mobile-web-app-title" content="Best Lap" />
      <meta name="application-name" content="Best Lap" />
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />
      <meta name="msapplication-TileColor" content="#13cf13" />
      <meta name="msapplication-TileImage" content="/mstile-144x144.png?v=dLnKbgLL3G" />
      <meta name="theme-color" content="#000" />
      <meta property="og:url"                content="https://www.bestlap.ee/" />
      <meta property="og:type"               content="website" />
      <meta property="og:title"              content="Best Lap" />
      <meta property="og:description"        content="2023 Racing Series" />
      <meta property="og:image"              content="https://www.bestlap.ee/social/2023/og-c.jpg" />
      <meta property="fb:app_id" content="557239605176820" />
      <title>Best Lap</title>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=dLnKbgLL3G" />
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=dLnKbgLL3G" />
      <link rel="icon" type="image/png" sizes="192x192" href="/android-chrome-192x192.png?v=dLnKbgLL3G" />
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=dLnKbgLL3G" />
      <link rel="manifest" href="/site.webmanifest?v=dLnKbgLL3G" />
      <link rel="mask-icon" href="/safari-pinned-tab.svg?v=dLnKbgLL3G" color="#13cf13" />
      <link rel="shortcut icon" href="/favicon.ico?v=dLnKbgLL3G" />
      <link rel="stylesheet" type="text/css" href="/patch.css" />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
