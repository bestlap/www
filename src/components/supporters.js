import * as React from "react"

export default function Supporters () {
    return (<ul className="supporters">
        <li><a href="https://www.magma.ee/" className="image"><img src="/media/magma.png" alt="Magma" /></a></li>
        <li><a href="https://www.liqui-moly.ee/" className="image"><img src="/media/liquimoly.png" alt="Liqui Moly" /></a></li>
        <li><a href="https://pitstop.direct/" className="image"><img src="/media/pitstop-direct.png" alt="Pitstop Direct" /></a></li>
        <li><a href="http://www.stalem.ee/" className="image"><img src="/media/stalem.png" alt="Stalem Performance" /></a></li>
        <li><a href="https://www.kruze.ee/" className="image -kruze"><img src="/media/kruze.png" alt="Kruze Disain" /></a></li>
        <li><a href="https://www.napalmkitchen.ee/" className="image"><img src="/media/napalm-kitchen.png" alt="Napalm Kitchen" /></a></li>
      </ul>);
}
