import { milliseconds } from './_helpers.js';
import { sort } from './time/preferences.js';

export default function Index (race, racers, classes, cars, statuses, rounds = [], laps = [], points = 0, sorting = false, by = undefined, asc = false) {
    const Index = {
        ...race,
        classes: {},
    };
    const pilots = [];

    if (!('minimum' in Index.rules)) {
        Index.rules.minimum = 2;
    }

    cars.map(car => car.number.map(number => {
        if (!racers[number]) {
            return car;
        }

        pilots[number] = {
            driver: racers[number],    
            car: car,
        };

        if (number in statuses) {
            pilots[number] = {
                ...pilots[number],
                paid: statuses[number].paid,
                checked: statuses[number].checked,
                active: statuses[number].active,
                grouped: statuses[number].grouped,
            }
        }

        return car;
    }));

    if (rounds.length > 0) {
        rounds.map((round, index) => Object.keys(round).map(number => {
            if (!(number in pilots)) {
                return number;
            }
            if (!('rounds' in pilots[number])) {
                pilots[number].rounds = [];
            }
            while (pilots[number].rounds.length < index) {
                pilots[number].rounds.push(null);
            }

            pilots[number].rounds.push(round[number]);

            const sorted = [...pilots[number].rounds].filter(entry => entry).sort((a, b) => milliseconds(a) > milliseconds(b) ? 1: -1);

            pilots[number].bestlap = sorted.length < race.rules.minimum ? '' : sorted[0];

            return number;
        }));
    }

    if (laps.length > 0) {
        laps.map((rounds, index) => {
            rounds.map(round => Object.keys(round).map(number => {
                if (!(number in pilots)) {
                    return round;
                }

                if (!('laps' in pilots[number])) {
                    pilots[number].laps = [];
                }

                if (!pilots[number].laps[index]) {
                    while (pilots[number].laps.length <= index) {
                        pilots[number].laps.push([]);
                    }
                }

                pilots[number].laps[index].push(round[number]);

                return round;
            }));

            return rounds;
        });
    }

    for (var classen in classes) {
        Index.classes[classen] = [];

        classes[classen].forEach(fillIndexWithPilots);
    }

    if (sorting) {
        const sorted = sort({...Index}, by, asc);

        for (const classen in sorted.classes) {
            sorted.classes[classen].map((pilot, index) => pilot.place = 'rounds' in pilot ? index + 1 : Infinity);
        }

        return sorted;
    }

    function fillIndexWithPilots (number) {
        const entry = pilots[number];

        if (typeof entry === 'undefined') {
            return;
        }

        const found = points[classen]?.find(point => point[0] === number);

        entry.number = number;
        entry.points = found ? found[1] : 0;

        Index.classes[classen].push(entry);
    }

    return Index;
}