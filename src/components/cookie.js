import React, { useState } from 'react';
import Cookies from 'universal-cookie';
import { useLocation } from "@reach/router" // this helps tracking the location
import { initializeAndTrack } from '@mobile-reality/gatsby-plugin-gdpr-cookies'
import { localize } from "../content";

export const useCookie = (key, options = {}) => {
  const cookies = new Cookies();
  const [item, setItemValue] = useState(() => {
    if (cookies.get(key)) {
      return cookies.get(key);
    }

    return null;
  });

  const setValue = (value, options) => {
    setItemValue(value);

    cookies.set(key, value, options);
  };

  const removeItem = () => {
    cookies.remove(key);
  };

  return [item, setValue, removeItem];
};

export default function Cookie ({
  lang,
}) {
  const location = useLocation();
  const [consent, setConsent] = useCookie('consent');
  const yes = () => {
    setConsent('true', { path: '/' })

    initializeAndTrack(location);
  }
  const no = () => {
    setConsent('declined', { path: '/' })
  }
 
  return consent === null && (
    <div className="cookie">
      <p>{localize('disclaimer', lang)}</p>
      <div className="bar">
        <button onClick={yes}>{localize('yes', lang)}</button>
        <button onClick={no}>{localize('no', lang)}</button>
      </div>
    </div>
  );
};
