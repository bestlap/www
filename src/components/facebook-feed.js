import React from 'react';
import Helmet from 'react-helmet';

import { useCookie } from './cookie';

export default function FacebookFeed () {
    const [consent] = useCookie('consent');

    return consent === 'true' ? (
        <>
            <Helmet>
                <script async defer crossOrigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v13.0" nonce="BAxVCbLv"></script>
            </Helmet>
            <div className="fb-page" data-href="https://www.facebook.com/bestlap.ee" data-tabs="timeline" data-width="480" data-height="620" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/bestlap.ee" className="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bestlap.ee">facebook.com/bestlap.ee</a></blockquote></div>
        </>
    ) : (
        <a href="https://www.facebook.com/bestlap.ee">facebook.com/bestlap.ee</a>
    );
}
