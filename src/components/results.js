import React, { useState } from 'react';
import { localize } from "../content";
import { romify, mark, order } from './_helpers';

const capitalize = str => str; // removed to avoid capitalization of names when reading from an object

const removeLowestValue = arr => [...arr.slice(0, arr.indexOf(Math.min(...arr))), ...arr.slice(arr.indexOf(Math.min(...arr)) + 1)];
const getRound = data => removeLowestValue(Object.keys(data).filter(round => Number.isInteger(parseInt(round))).map(round => data[round])).sort((a, b) => b - a);
const compareArrays = (a, b, asc) => {
    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
          return (a[i] > b[i]) && asc ? 1 : -1;
        }
      }

      return 0;
}
const sort = (units, column, asc) => units.sort((a, b) => {
    switch (column) {
        case 'total':
            const difference = a.data.total - b.data.total;

            if (difference === 0) {
                const rounds = {
                    a: getRound(a.data),
                    b: getRound(b.data),
                };
                const compared = compareArrays(rounds.a, rounds.b, asc);

                return compared;
            }

            return difference * (asc ? 1 : -1);

        case 'name':

            return (a.name < b.name ? -1 : 1) * (asc ? 1 : -1);

        case 'car':
            const model = cars => cars[Object.keys(cars)[Object.keys(cars).length - 1]].model;

            return 'cars' in a.data ? (model(a.data.cars) < model(b.data.cars) ? -1 : 1) * (asc ? 1 : -1) : 0;

        default:

            return ((a.data[column] || 0) - (b.data[column] || 0)) * (asc ? 1 : -1);
    }
});

export default class ResultsTable extends React.Component {
    state = {
        calculated: [],
    };

    constructor (props) {
        super(props);
        
        this.dates = props.dates;
        this.lang = props.lang

        this.headings = ['name', 'car', ...this.dates.map((date, index) => index + 1), 'total'];
    
        this.columns = [
            'points',
            'cars',
            'racers',
            'teams',
        ];
      }

    componentDidMount() {
        this._asyncRequest = renderResults(this.dates, this.columns, this.lang).then(
            calculated => {
                this._asyncRequest = null;

                this.setState({ calculated });
            }
        );
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    render() {
        return this.state.calculated.length < 1 ? (<div className="loading"></div>) : <Table lang={this.lang} dates={this.dates} data={this.state.calculated} headings={this.headings} />;
    }
}

async function renderResults (dates, columns, lang) {
    const state = await columnize(dates, columns);
    const pilots = new Pilots(state.racers);
    const points = new Points(state.points);
    const teams = new Teams(state.teams, points, dates.length);
    const results = new Results(pilots, points, state.cars, dates.length);
    const teamed = Object.keys(teams);
    const out = [];

    if (teamed.length > 0) {
        const result = {
            type: 'team',
            name: localize('results-team', lang),
            units: sort(teamed.filter(name => 'total' in teams[name]).map((name, index) => ({
                name,
                place: index + 1,
                data: teams[name],
                dates
            })), 'total', 0),
        };
        
        out.push({
            ...result,
            units: result.units.map((unit, index) => ({
                ...unit,
                place: index + 1,
            }))
        });
    };

    for (let classen in results) {
        const classed = Object.keys(results[classen]);

        if (classed.length <= 0) {
            continue;
        };

        const result = {
            type: 'class',
            name: classen,
            units: sort(classed.map((pilot, index) => ({
                name: pilot,
                place: index + 1,
                data: results[classen][pilot],
                dates
            })), 'total', 0),
        };

        out.push({
            ...result,
            units: result.units.map((unit, index) => ({
                ...unit,
                place: index + 1,
            }))
        });
    }

    return out;
}

async function columnize (dates, columns) {
    const columnized = {};
    const loaded = {};
    const random = '?' + (Math.random() + 1).toString(36).substring(7);

    await Promise.all(columns.map(async column => {
        loaded[column] = {};
        columnized[column] = [];

        await Promise.all(dates.map(async date => loaded[column][date] = await fetch('/time/' + date + '/' + column + '.json' + random).then(async response => await response.json())));

        Object.keys(order(loaded[column])).map(date => columnized[column].push(loaded[column][date]));
    }));

    return columnized;
}

function Table ({ lang, dates, data, headings }) {
    const [out, setOut] = useState(data);

    const click = event => {
        const th = event.target;
        const sorted = 'sort' in th.dataset ? th.dataset.sort : 'sortDefault' in th.dataset ? th.dataset.sortDefault === 'asc' ? 'desc' : 'asc' : 'desc';
        const asc = sorted !== 'asc';
    
        Array.prototype.forEach.call(th.parentElement.children, function (th) {
            delete th.dataset.sort;
        })
    
        th.dataset.sort = asc ? 'asc' : 'desc';

        setOut(out.map(group => ({
            ...group,
            units: sort(group.units, th.dataset.column || 'total', asc),
        })));
    };

    return (
        <table className="results">
            <thead>
                <tr>
                    <th role="button" aria-label="Sort by name" className="name" onClick={click} data-column="name">{localize('results-name', lang)}</th>
                    <th role="button" aria-label="Sort by car" className="car" onClick={click} data-column="car">{localize('results-car', lang)}</th>
                    {dates.map((date, index) => (
                        <th role="button" aria-label={`Sort by round ${index + 1}`} key={index + 1} data-sort-default="desc" className={'round ' + (index + 1)} data-column={index} onClick={click}>{romify(index + 1)}</th>
                    ))}
                    <th role="button" aria-label="Sort by total" data-sort-default="desc" data-sort="desc" className="total" data-column="total" onClick={click}></th>
                </tr>
            </thead>
            {out.map((group, index) => (
                <Group name={group.name} key={index} dates={dates} headings={headings}>
                    {group.units.map((unit, index) => (
                        group.type === 'team' ?
                        <Team name={unit.name} place={unit.place} data={unit.data} key={unit.name} dates={dates} />
                        :
                        <Pilot name={unit.name} place={unit.place} data={unit.data} key={unit.name} dates={dates} />
                    ))}
                </Group>
            ))}
        </table>
    );
}

function Group (props) {
    return (
    <>
        <thead>
            <tr>
                <th colSpan={props.headings.length}>
                    {props.name}
                </th>
            </tr>
        </thead>
        <tbody>
            {props.children}
        </tbody>
    </>
    );
}

function Pilot (props) {
    const classList = ['pilot'];
    const numbers = [];

    for (let number in props.data.cars) {
        numbers.push(number);
    }

    const car = props.data.cars[numbers[numbers.length - 1]];
    const marked = '/logos/' + mark(car.mark) + '.png';

    return (
    <tr data-place={props.place} className={classList.join(' ')}>
        <td className="name">
            <div className="sequence">
            <span>{props.name}</span>
            <div className="numbers">
            {numbers.map((number, index) => <figure className="number" key={index} id={number}><figcaption>{number}</figcaption></figure>)}
            </div>
            </div>
        </td>
        <td className="car">
            <figure className="mark">
                <figcaption>
                    <span className="mark">{car.mark}</span>
                    <span className="model">{car.model}</span>
                </figcaption>
                <img src={marked} alt={car.mark} />
            </figure>
        </td>
        {props.dates.map((date, index) => (
            <td className={'round ' + (index + 1)}  key={index + 1}>{props.data[index]}</td>
        ))}
        <td className="total">{props.data.total}</td>
    </tr>
    );
}


function Pilots (racers) {
    var Pilots = {};

    racers.forEach(function (racer, index) {
        for (let number in racer) {
            if (isNaN(number)) {
                continue;
            }
            var name = racer[number];

            if (!exists(name)) {
                Pilots[name] = [];
            }
            if (Pilots[name].indexOf(parseInt(number)) > -1) {
                continue;
            }
            Pilots[name].push(parseInt(number));
        }
    });

    function exists (name) {
        var exists = false;

        for (let pilot in Pilots) {
            if (name.toLowerCase() === pilot.toLowerCase()) {
                if (name !== pilot) {
                    Object.defineProperty(Pilots, name,
                        Object.getOwnPropertyDescriptor(Pilots, pilot));
                    delete Pilots[pilot];
                }
                exists = true;

                break;
            }
        }

        return exists;
    }
    
    return Pilots;
}

function Points (races) {
    var Points = {};

    races.forEach(function (race, index) {
        for (let classen in race) {
            if (!(classen in Points)) {
                Points[classen] = {};
            }

            for (let result in race[classen]) {
                var number = race[classen][result][0];
                var points = race[classen][result][1];

                if (!(number in Points[classen])) {
                    Points[classen][number] = {};
                }

                Points[classen][number][index] = points;
            }
        }
    });

    return Points;
}

function Team (props) {
    const classList = ['team'];

    return (
    <tr data-place={props.place} className={classList.join(' ')}>
        <td className="name" colSpan="2">
            <div className="sequence">
            <span>{props.name}</span>
            <div className="numbers">
            {props.data.numbers.map((number, index) => <a href={'#' + number} key={index} aria-label={number}><figure className="number"><figcaption>{number}</figcaption></figure></a>)}
            </div>
            </div>
        </td>
        {props.dates.map((date, index) => (
            <td className={'round ' + (index + 1)}  key={index + 1}>{props.data[index]}</td>
        ))}
        <td className="total">{props.data.total}</td>
    </tr>
    );
}

function Teams (teams, points, stages) {
    const Teams = {};
    
    const reduced = teams.map(round => Object.keys(round).filter(name => name !== "").map(key => capitalize(key)).filter((item, pos, self) => self.indexOf(item) === pos).sort());

    reduced.map(round => round.map(name => Teams[capitalize(name)] = {}));

    teams.map((round, roundIndnext) => Object.keys(round).filter(name => name !== "").map((name, teamIndex) => {
        if (!('numbers' in Teams[capitalize(name)])) {
            Teams[capitalize(name)].numbers = [];
        }

        Teams[capitalize(name)].numbers = [...new Set([...Teams[capitalize(name)].numbers, ...round[name]])];

        return round;
    }));

    Object.keys(points).map(classified => Object.keys(points[classified]).map(number => {
        Object.keys(points[classified][number]).map((round, roundIndex) => {
            const team = reduced[parseInt(round)].find(name => Teams[capitalize(name)].numbers.includes(number));

            if (!team) {
                return round;
            }

            if (!(round in Teams[team])) {
                Teams[team][round] = 0;
            }

            if (!('total' in Teams[team])) {
                Teams[team].total = 0;
            }

            if (!teams[round][team]?.includes(number)) {
                return round;
            }

            Teams[team][round] += points[classified][number][round] || 0;
            Teams[team].total += points[classified][number][round] || 0;

            return round;
        });
        
        return number;
    }));

    return Teams;
}

function Results (pilots, points, cars, stages) {
    var Results = {};

    for (let classen in points) {
        Results[classen] = {};

        for (let pilot in pilots) {
            var total = [];

            if (!(pilot in Results[classen])) {
                
            }
            pilots[pilot].forEach(function (number) {
                if (!(number in points[classen])) {
                    return;
                }
                if (!(pilot in Results[classen])) {
                    Results[classen][pilot] = {};
                }
                for (let stage in points[classen][number]) {
                    if (!('cars' in Results[classen][pilot])) {
                        Results[classen][pilot].cars = {};
                    }
                    total.push(points[classen][number][stage] || 0);

                    Results[classen][pilot][stage] = points[classen][number][stage] || 0;
                    Results[classen][pilot].cars[number] = getCar(number);
                }
                Results[classen][pilot].total = total.length > 4 ? total.reduce((a, b) => a + b, 0) - total.find(a => a === Math.min(...total)): total.reduce((a, b) => a + b, 0);
            });
        }
    }

    function getCar (number) {
        var car = undefined;

        cars.forEach(function (round) {
            if (!("length" in round)) {
                return;
            }

            round.forEach(function(c) {
                if (typeof car !== 'undefined') {
                    return;
                }
                c.number.forEach(function (n) {
                    if (typeof car !== 'undefined') {
                        return;
                    }
                    if (n === number) {
                        car = c;
                    }
                });
            })
        });

        return car;
    }

    return Results;
}
