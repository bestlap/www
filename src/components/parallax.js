
import React, { useEffect, useRef } from "react";

export default function Parallax () {
    const reference = useRef();

    useEffect(() => {
        const viewroll = document.querySelector('.everything');

        const parallax = () => {
            if (!reference.current) {
                return;
            }

            const image = reference.current.querySelector('img');
            const growth = -120 + 10 * (image.offsetTop - window.scrollY) / window.innerHeight;

            if (window.scrollY < viewroll.offsetHeight - window.innerHeight) {
                image.style.transform = 'matrix(1, 0, 0, 1, -500, ' + (growth) +') skew(15deg)';
            }
        }
         
        window.onscroll = parallax;

        parallax();
    },

    []);

    return (
        <div className="parallax" ref={reference}>
            <img src="/media/89432500_223149805497701_2582585215431999488_n.jpg" alt="Snowwhite" />
        </div>
    );
}
