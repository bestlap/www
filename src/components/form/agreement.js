import React from 'react';

const Agreement = ({
    name,
    label,
    ...props
}) => (
    <div>
        <input type="checkbox" name={name} id={name} {...props} />
        <label htmlFor={name}>
            {label}
        </label>
    </div>
);

export default Agreement;
