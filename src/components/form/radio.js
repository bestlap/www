import React from 'react';
import { useField, useFormikContext } from 'formik';

export default function Radio ({
    id,
    name,
    label,
    units,
    onChange,
    changed,
    setFieldValue,

    ...props
}) {
    const { values } = useFormikContext();
    const [ field ] = useField({ name, ...props });
    const update = event => {
      changed && changed({...values, [name]: event.target.value}, setFieldValue, name);

      onChange && onChange(event);
    };

    return (
        <div>
            <input type="radio" {...field} {...props} id={id} onChange={update} />
            <label htmlFor={id}>{label}</label>
        </div>
    );
};
