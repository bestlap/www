import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

import Select from './select';

export default function ClassSelect ({
    label,
    reject,
    connected,
    setFieldValue,

    ...props
}) {
    const { values } = useFormikContext();

    useEffect(() => {
        connected && connected(values, setFieldValue, 'class');
    }, [values]);

    return (
        <div>
            <label>
                {label}
            </label>
            <div>
                <Select reject={reject} {...props}>
                    <option value="Street A">Street A</option>
                    <option value="Street B">Street B</option>
                    <option value="Street C">Street C</option>
                    <option value="Unlim 2000">Unlim 2000</option>
                    <option value="Unlim 2000+">Unlim 2000+</option>
                </Select>
            </div>
        </div>
    );
};
