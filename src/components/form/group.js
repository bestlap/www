import React from 'react';

const Group = ({
    name,
    children,
}) => (
    <div role="group" aria-labelledby={name} className={name}>
        {children}
    </div>
);

export default Group;
