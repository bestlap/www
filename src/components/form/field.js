import React from 'react';
import { useField, useFormikContext } from 'formik';

export default function Field ({
    name,
    label,
    onChange,
    changed,
    setFieldValue,
    ...props
}) {
    const { values } = useFormikContext();
    const [ field ] = useField({ name, ...props });
    const update = event => {
      changed && changed({...values, [name]: event.target.value}, setFieldValue, name);

      onChange && onChange(event)
    };

    return (
        <div>
            <input {...field} {...props} onChange={update} id={name} />
            <span>{props.units || null}</span>
        </div>  
    );
};
