import React from 'react';
import { useField } from 'formik';

export default function Checkbox ({ label, ...props }) {
  const [field, meta, helpers] = useField(props);

  return (
    <div>
      <label htmlFor={props.name}>
        {label}
      </label>
      <div>
        <input {...field} {...props} id={props.name} />
        <span>{props.units}</span>
      </div>
      {meta.touched && meta.error ? (
        <p className="-attention">{meta.error}</p>
      ) : null}
      <p>
          {props.hint ? props.hint : null}
      </p>
    </div>
  );
};
