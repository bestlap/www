import React from "react";
import { Field } from "formik";

const Select = ({ rejected, ...props }) => (
  <Field {...props}>
    {({ field, form }) => (
      <select {...field}>
        {props.children.map((option, index) => (
          <option
            key={index}
            value={option.props.value}
            disabled={rejected.includes(option.props.value)}
          >
            {option.props.children}
          </option>
        ))}
      </select>
    )}
  </Field>
);

export default Select;
