import React from 'react';
import {useFormikContext} from 'formik';

export default function Input ({
    name,
    label,
    hint,
    children
}) {
    const {errors, touched} = useFormikContext();
    const getError = () => {
        if (errors[name] && touched[name]) {
            return errors[name];
        }

        return null;
    };

    return (
        <div className={name}>
            <label htmlFor={name}>{label}</label>

            {children}

            <p className="-attention">{getError()}</p>

            <p>{hint || null}</p>
        </div>
    );
};
