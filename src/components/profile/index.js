import React, { useEffect, useState } from 'react';
import { navigate } from 'gatsby'
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw'

import Login from '../login';
import { authorize, get, login, logout } from "../../services/api";

const REDIRECT = '/registration';

const STAGES = {
    'initialization': 0,
    'authorization': 1,
    'login': 2,
    'loading' : 3,
    'success': 4,
};

export default function Profile ({
    lang,
}) {
    const [stage, setStage] =  useState(STAGES.initialization);
    const [authorized, setAuthorized] = useState(false);
    const [profile, setProfile] = useState(null);
    const [error, setError] = useState(null);

    const logon = (values, { setSubmitting }) => {
        setError(false);

        login(values.email, values.password).then(() => {
            setSubmitting(false);
            setAuthorized(true);
            setStage(STAGES.loading)
        }).catch((rejection) => {
            setSubmitting(false);
            console.warn(rejection.reason);
            setError(rejection.reason);
            setStage(STAGES.login)
        });
    };

    const logoff = () => {
        logout().then(() => {
            setAuthorized(false);
            setStage(STAGES.login)
        }).catch((rejection) => {
            console.warn(rejection.reason);
        });
    };

    const stages = {
        [STAGES.initialization]: () => (
            <>
                Initializating...
            </>
        ),
        [STAGES.authorization]: () => (
            <>
                Authorizing...
            </>
        ),
        [STAGES.login]: () => (
            <>
                <Login submit={logon} error={error} />
            </>
        ),
        [STAGES.loading]: () => (
            <>
                Authorized! Loading data...
            </>
        ),
        [STAGES.success]: () => (
            <>
                <ReactMarkdown rehypePlugins={[rehypeRaw]} children={profile.message} />
                <br />
                <br />
                <br />
                <div>
                    <button type="button" onClick={event => logoff()}>Logout</button>
                </div>
            </>
                
        ),
    };

    useEffect(() => {
        setStage(STAGES.authorization);

        try {
            authorize().then(response => {
                if (response.status !== 'accepted') {
                    setStage(STAGES.login);

                    console.warn(response.reason);

                    return;
                }

                setAuthorized(true);
            }).catch((rejection) => {
                console.warn(rejection.reason);
                setStage(STAGES.login)
            });
        }
        
        catch (rejection) {
            console.warn(rejection.reason);
        }
    }, []);

    useEffect(() => {
        if (!authorized) {
            return;
        }

        try {
            setStage(STAGES.loading);

            get('profile').then(response => {
                if (response.status !== 'accepted') {
                    console.warn(response.reason);

                    return;
                }

                if (!response.profile) {
                    console.warn('No profile data');

                    return;
                }

                if (!response.profile.message) {
                    navigate(REDIRECT);
        
                    return;
                }

                setStage(STAGES.success);
                setProfile(response.profile);
                
            }).catch((rejection) => {
                console.warn(rejection.reason, rejection);
            });
        }
        
        catch (rejection) {
            console.warn(rejection.reason);
        }
    }, [authorized]);

    useEffect(() => {

    }, [stage]);

    return stages[stage]();
}
