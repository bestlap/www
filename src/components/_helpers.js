export const capitalize = name => name.split(' ').map((word, index) => {
    switch (word.toLowerCase()) {
        case 'a':
        case 'at':
        case 'but':
        case 'for':
        case 'in':
        case 'or':
        case 'of':
        case 'per':
        case 'the':

            return index === 0 ? word.toLowerCase().charAt(0).toUpperCase() + word.toLowerCase().slice(1) : word.toLowerCase();
        
        default:
            
            return word.toLowerCase().charAt(0).toUpperCase() + word.toLowerCase().slice(1);
    }
}).join(' ');

export const merge = array => array.reduce((previous, current) => [...previous, ...current]);

const Helpers = {
    capitalize,
    merge,
    romify,
    mark,
    milliseconds,
    format,
    pad,
    debounce,
    throttle,
};

export default Helpers;

export function romify (num) {
    if (typeof num !== 'number') {
        return false;
    }
    var digits = String(+num).split("");
    var key = [
        "","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
        "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
        "","I","II","III","IV","V","VI","VII","VIII","IX"
    ];
    var roman_num = "",
    i = 3;
    while (i--) roman_num = (key[+digits.pop() + (i * 10)] || "") + roman_num;

    return Array(+digits.join("") + 1).join("M") + roman_num;
}

export function mark (name) {
    var mark = name.toLowerCase().replace(' ', '-');

    switch (mark) {
        case 'vw':
            mark = 'volkswagen';

            break;

        default:

            break;
    }

    return mark;
}

export function milliseconds (formatted, hours) {
    if (typeof formatted !== 'string') {
        return Infinity;
    }
    if (formatted === String()) {
        return 0;
    }
    var time = formatted.split(':');
    if (typeof (hours ? time[2] : time[1]) === 'undefined') {
        return 0;
    }
    var hour = hours ? time[0] * 60 * 60 * 1000 : 0;
    var minute = (hours ? time[1] : time[0]) * 60 * 1000;
    var second = (hours ? time[2] : time[1]).split(',');
    var roundsecond = second[0] * 1000
    var millisecond = parseInt(pad(second[1], 3, true));
    
    return hour + minute + roundsecond + millisecond;
}


export function format (milliseconds, hours) {
    if (!(parseInt(milliseconds) >= 0)) {
        return '--:--,--';
    }
    var remainer = milliseconds;
    var millisecond = remainer % 1000;
    remainer = (remainer - millisecond) / 1000;
    var roundsecond = remainer % 60;
    remainer = (remainer - roundsecond) / 60;
    var minute = remainer % 60;
    var hour = (remainer - minute) / 60;

    var countMillisecond = pad(Math.round(millisecond / 10));

    if (countMillisecond === '100') {
        roundsecond = roundsecond + 1;
        countMillisecond = '00';
    } 

    return (hours ? pad(hour) + ":" : "") + pad(minute) + ':' + pad(roundsecond) + ',' + countMillisecond;
}

export function pad (number, size, right) {
    var result = number.toString();
    while (result.length < (size || 2)) result = (right ? "": "0") + result + (right ? "0": "");
    return result.substring(0, right ? (size || 2) : number.toString().lenght);
}

export function debounce (action, delay) {
    let timer;
    
    try {
        clearTimeout(timer);
    } catch (exception) {
        // console.error('Timer not set:', exception);
    }
    
    timer = setTimeout(action, delay);
}

export function throttle (action, delay) { 
    let timer;

    return function () {
        if (timer) {
            return;
        }

        timer = true;

        action.call();

        setTimeout(() => timer = false, delay);
    };
}

export function order (unordered) {
    return Object.keys(unordered).sort().reduce(
        (ordered, key) => { 
            ordered[key] = unordered[key]; 
          return ordered;
        },
        {}
    );
}

/*

// Typescript

export function debounce(action: () => void, delay: number): void {
    let timer: ReturnType<typeof setTimeout> | undefined;
    
    try {
        if (timer) {
            clearTimeout(timer);
        }
    } catch (exception) {
        // console.error('Timer not set:', exception);
    }
    
    timer = setTimeout(action, delay);
}

export function throttle(action: () => void, delay: number): void {
    let timer: ReturnType<typeof setTimeout> | undefined;

    if (timer) {
        return;
    }
    timer = setTimeout(function () {
        action();
        timer = undefined;
    }, delay);
}
*/
