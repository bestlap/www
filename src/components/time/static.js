import React, { useState } from 'react';

import Table from './table';
import Modal from '../modal';
import Laps from './laps';

// import { sort } from './preferences';

export default function Time (props) {
    const { lang, index, onClick } = props;
    const click = laps => {
        if (typeof onClick !== 'function') {
            return;
        }

        onClick(laps);
    };
    // const [index, setIndex] = useState(props.index)

    /*
    const resort = by => setIndex(sort({...index}, by));
    const sorters = ['bestlap', 'bestlaps', 'mean', 'points'];
    */

    return (
        <>
            {/*}
            <header>
                <h2>
                    {index.track}
                    </h2>
                <p>
                    {index.place} {index.date}
                </p>
            </header>
            
            <div className="buttonbar">
                {sorters.map(sorter => <button className="button" onClick={() => resort(sorter)}>{sorter}</button>)}
            </div>
            {/*/}
            {console.log('index', index)}
            <Table lang={lang} index={index} onClick={click} />
        </>
    );
}
