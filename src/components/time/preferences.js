import { io } from 'socket.io-client';

import { milliseconds } from '../_helpers.js';

import Index from '../index.js';

export const socket = io('https://bestlap.herokuapp.com/', { secure: true, transports: ['websocket'] });
// export const socket = io('http://localhost:5000/', { transports: ['websocket'] });

export const json = [
    'race',
    'racers',
    'cars',
    'classes',
    'laps',
    'rounds',
    'points',
    'messages',
    'numbers',
    'statuses',
];

const points = (a, b) => a.points < b.points ? -1 : a.points > b.points ? 1 : 0;
const participation = (a, b) => !('rounds' in a) && !('rounds' in b) ? 0 : 'rounds' in a && !('rounds' in b) ? 1 : !('rounds' in a) && 'rounds' in b ? -1 : Infinity;
const consistency = (a, b, minimum) => {
    const x = a.rounds.filter(x => x).length;
    const y = b.rounds.filter(x => x).length;

    return x < minimum && y < minimum ? 0 : /*x >= minimum && y >= minimum ? (x < y ? -1 : x > y ? -1 : 0) : */x >= minimum && y < minimum ? 1 : x < minimum && y ? -1 : Infinity
};
const mean = (a, b) => {
    const x = count(a.rounds);
    const y = count(b.rounds);

    return x.sum / x.total < y.sum / y.total ? 1 : -1;

    function count (rounds) {
        const count = {
            sum: 0,
            total: 0,
        };

        rounds.map(bestlap => {
            if (typeof bestlap !== 'string' || bestlap === '' || bestlap === '--:--,--') {
                return bestlap;
            }

            count.sum += milliseconds(bestlap);
            count.total++;

            return bestlap;
        });

        return count;
    }
}
const bestlap = (a, b) => milliseconds(a.bestlap) < milliseconds(b.bestlap) ? 1 : milliseconds(a.bestlap) > milliseconds(b.bestlap) ? -1 : 0;
const bestlaps = (a, b, minimum) => {
    const x = {
        total: 0,
        rounds: [...a.rounds].filter(round => round).sort((a, b) => milliseconds(a) - milliseconds(b)),
    };

    const y = {
        total: 0,
        rounds: [...b.rounds].filter(round => round).sort((a, b) => milliseconds(a) - milliseconds(b)),
    };

    for (let lap = 0; lap < minimum; lap++) {
        x.total += milliseconds(x.rounds[lap]);
        y.total += milliseconds(y.rounds[lap]);
    }

    return x.total < y.total ? 1 : x.total > y.total ? -1 : bestlap(a, b);
}

export const sort = (index, by = 'points', asc = false) => {
    Object.keys(index.classes).map(name => index.classes[name].sort((a, b) => {
        const participated = participation(a, b);

        if (participated < Infinity) {
            return participated * (asc ? 1 : -1);
        }

        const consitent = consistency(a, b, index.rules.minimum);

        if (consitent < Infinity) {
            return consitent * (asc ? 1 : -1);
        }

        switch (by) {
            case 'bestlap':

                return bestlap(a, b) * (asc ? 1 : -1);

            case 'points':
                const result = points(a, b);

                if (result === 0) {
                    return bestlaps(a, b, index.rules.minimum) * (asc ? 1 : -1);
                }

                return result * (asc ? 1 : -1);

            case 'bestlaps':

                return bestlaps(a, b, index.rules.minimum) * (asc ? 1 : -1);

            case 'numbers':

                return (a.number - b.number) * (asc ? 1 : -1);

            case 'mean':
            default:

                return mean(a, b) * (asc ? 1 : -1);
        }
    }));

    return index;
};

export async function load (json) {
    const loaded = {};
    const random = '?' + (Math.random() + 1).toString(36).substring(7);

    await Promise.all(json.map(async file => {
        loaded[file] = await fetch('/' + file + '.json' + random).then(response => response.json());
    }));
    
    loaded.index = Index(loaded.race, loaded.racers, loaded.classes, loaded.cars, loaded.statuses, loaded.rounds, loaded.laps, loaded.points, true, 'numbers', true);

    return loaded;
}

export const ideally = [
    {
        'Street A': [
            {
                number: 100,
                name: 'Vladimir G',
                car: {
                    mark: 'Mazda',
                    model: '3',
                    volume: 1598,
                },
                rounds: [
                    '01:42,21',
                    '01:42,30',
                    '01:43,07',
                ],
                laps: [
                    [],
                    [],
                    [],
                ],
                points: 5,
            },
        ]
    },
];
