import React from 'react';

import Queue from './queue';
import Table from './table';

import { socket, json, load, sort } from './preferences';

import { milliseconds } from '../_helpers';

export default class Time extends React.Component {
    state = {
        calculated: [],
    };

    constructor (props) {
        super(props);
        
        this.lang = props.lang;
        this.live = props.live || false;
      }

    componentDidMount() {
        this._asyncRequest = load(json).then(
            calculated => {
                this._asyncRequest = null;

                this.setState({ calculated });
            }
        );

        socket.on('record', (record, id) => {
            if (id === socket.id) {
                return;
            }

            if (!record) {
                return;
            }

            console.log('record', record, id);
            console.log('state', this.state);

            const updated = {...this.state.calculated.index};

            Object.keys(record.numbers).map(number => {
                if (!Object.keys(this.state.calculated.racers).includes(number)) {
                    return number;
                }
                
                const name = Object.keys(this.state.calculated.index.classes).find(name => this.state.calculated.index.classes[name].find(pilot => pilot.number === parseInt(number)));
                const pilotIndex = this.state.calculated.index.classes[name].findIndex(pilot => pilot.number === parseInt(number));
                const lapIndex = record.lap - 1;
                const roundIndex = record.round - 1;
            
                const updatedLaps = [...this.state.calculated.index.classes[name][pilotIndex]?.laps || []];
                const updatedRounds = [...this.state.calculated.index.classes[name][pilotIndex]?.rounds || []];

                if (!updatedLaps[roundIndex]) {
                    updatedLaps[roundIndex] = [];
                }

                updatedLaps[roundIndex][lapIndex] = record.numbers[number];
                updatedRounds[roundIndex] = !('rounds' in this.state.calculated.index.classes[name][pilotIndex]) || milliseconds(record.numbers[number]) < milliseconds(this.state.calculated.index.classes[name][pilotIndex].rounds[roundIndex]) ? record.numbers[number] : this.state.calculated.index.classes[name][pilotIndex].rounds[roundIndex];

                updated.classes[name][pilotIndex].laps = updatedLaps;
                updated.classes[name][pilotIndex].rounds = updatedRounds;
                updated.classes[name][pilotIndex].bestlap = milliseconds(record.numbers[number]) < milliseconds(this.state.calculated.index.classes[name][pilotIndex].bestlap) ? record.numbers[number] : this.state.calculated.index.classes[name][pilotIndex].bestlap;

                return number;
            });

            this.setState({ ...this.state.calculated, index: sort(updated, 'numbers', true) });
        });
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
        
        socket.off('record');
    }

    render() {
        const out = this.state.calculated;
        const { race, index } = out;
        const click = laps => {
            if (this.props.onClick && typeof this.props.onClick === 'function') {
                this.props.onClick(laps);
            }

            this.props.onClick(laps);
        };

        return (
            out.length < 1 ?
            (
                <div className="loading"></div>
            ) : 
            (
                <>
                    {!this.live && `${race.place} ${race.date}`}
                    <Queue />
                    <Table lang={this.lang} rules={race.rules} index={index} realtime={true} onClick={click} />
                </>
            )
        );
    }
}
