import React from 'react';

import Class from './class';
import Pilot from './pilot';

export default function Table ({ lang, index, realtime, onClick }) {
    const headers = [
        'number',
        'driver',
        'mark',
        'model',
        'volume',
        'turbo',
    ];
    const click = laps => {
        if (typeof onClick !== 'function') {
            return;
        }

        onClick(laps);
    };

    return (
        <table data-realtime={realtime ? 'yes' : 'no'}>
            {Object.keys(index.classes).map(name => (
                <Class name={name} key={name} columns={headers.length + index.rules.rounds} realtime={realtime}>
                    {index.classes[name].filter(entry => 'rounds' in entry && (!('active' in entry) || entry.active === '1')).map(pilot => <Pilot lang={lang} data={pilot} rules={index.rules} headers={headers} key={pilot.number} realtime={realtime} onClick={click} />)}
                </Class>
            ))}
        </table>
    );
}
