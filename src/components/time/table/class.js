import React from 'react';

export default function Class ({ name, columns, realtime, children }) {
    return (
        <>
            <thead className="class">
                <tr>
                    <th colSpan={columns + realtime ? 2 : 0}>{name}</th>
                </tr>  
            </thead>
            <tbody>
                {children}
            </tbody>
        </>
    );
}
