import React from 'react';
import Cell from './cell';

import { mark } from '../../_helpers';

export default function Pilot ({ lang, data, rules, headers, realtime, onClick }) {
    const { number, driver, car, rounds, laps, bestlap, points, place } = data;
    const columns = [];
    const click = () => {
        if (typeof onClick !== 'function') {
            return;
        }

        onClick(laps);
    };

    // console.time("answer time");

    console.log('realtime', realtime)

    if (!realtime) {
        console.log('still')
        columns.push({
            class: 'place',
            content: place,
        });
    }

    headers.map(header => {
        switch (header) {
            case 'number':
                columns.push({
                    class: header,
                    content: number,
                });

                break;
            
            case 'driver':
                columns.push({
                    class: header,
                    content: driver,
                });

                break;
            
            case 'mark':
                columns.push({
                    class: header,
                    content: <img src={`/logos/${mark(car.mark)}.png`} alt={car.mark} />,
                });

                break;

            case 'model':
                columns.push({
                    class: header,
                    content: car.model,
                });

                break;

            case 'volume':
                columns.push({
                    class: header,
                    content: (car.engine.volume / 1000).toFixed(1),
                });

                break;

            case 'turbo':
                columns.push({
                    class: header,
                    content: car.engine.turbo ? 'T' : '',
                });

                break;

            default:
                
                columns.push({
                    class: header,
                    content: header,
                });

                break;
        }
        
        return header;
    });

    for (let round = 0; round < rules.rounds; round++) {
        columns.push({
            class: 'round' + (bestlap && bestlap === rounds[round] ? ' bestlap' : ''),
            content: rounds[round] || '--:--,--',
            extra: {
                'data-number': round + 1,

            }
        });
    }

    if (!realtime) {
        columns.push({
            class: 'points',
            content: points,
        });
    }

    // console.timeEnd("answer time");

    return (
        <tr className="racer points" data-place={place} data-points={points} onClick={click}>
            {columns.map((column, index) => <Cell className={column.class} key={column.class + index} {...('extra' in column ? column.extra : {})}>{column.content}</Cell>)}
        </tr>
    );
}
