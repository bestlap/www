import React from 'react';

export default function Laps ({ laps }) {
    console.log('laps', laps);
    return (
        <table className="laps-table">
            <thead>
                <tr>
                    <th>
                        I
                    </th>
                    <th>
                        II
                    </th>
                    <th>
                        III
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{laps[0] && laps[0].map((lap, lapIndex) => <div key={lapIndex}>{lap}</div>)}</td>
                    <td>{laps[1] && laps[1].map((lap, lapIndex) => <div key={lapIndex}>{lap}</div>)}</td>
                    <td>{laps[2] && laps[2].map((lap, lapIndex) => <div key={lapIndex}>{lap}</div>)}</td>
                </tr>
            </tbody>
        </table>
    )
}
