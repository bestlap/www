import React, { useState, useEffect } from 'react';

import { socket } from '../preferences';

import { debounce } from '../../_helpers';

// socket.emit('system message', `${window.navigator.appVersion}`);

export default class Queue extends React.Component {
    state = {
        calculated: null,
    };

    constructor (props) {
        super(props);
        
        this.lang = props.lang
      }

    componentDidMount() {
        this._asyncRequest = fetch('/numbers.json').then(response => response.json()).then(
            calculated => {
                this._asyncRequest = null;

                this.setState({ calculated });
            }
        );
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    render() {
        const numbers = this.state.calculated;

        return (
            numbers === null ?
            (
                <div className="loading"></div>
            ) : 
            (
                <Q numbers={numbers} />
            )
        );
    }
}


function Q ({ numbers }) {
    const [queue, setQueue] = useState(numbers);

    useEffect(() => {
        socket.on('requeue', (numbers, id) => id !== socket.id ? debounce(() => setQueue(numbers), 1) : undefined);
    
        return () => {
            socket.off('requeue');
        };
    }, []);
    
    return (
        <div className="queue">
            {Object.keys(queue).filter(pool => queue[pool].length > 0).map(pool => <ul className={pool} key={pool}>{queue[pool].map(number => <li key={number}>{number}</li>)}</ul>)}
        </div>
    );
}
