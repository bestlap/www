import React from 'react';
import { Formik } from 'formik';

import Input from './form/input';
import Field from './form/field';

export default function Login ({
    submit,
    error,
}) {
    return (
        <Formik
            initialValues={{
                email: '',
                password: ''
            }}

            validate={values => {
                const errors = {};
                
                if (!values.email) {
                    errors.email = 'Required';
                }
                
                else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(
                        values.email
                    )
                )
                
                {
                    errors.email = 'Invalid email address';
                }

                return errors;
            }}
            
            onSubmit={submit}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
            }) => (
                <form onSubmit={handleSubmit}>
                    <fieldset>
                        <legend>Login</legend>

                        <Input name="email" label="E-mail">
                            <Field
                                type='email'
                                name='email'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                        </Input>

                        <Input name="password" label="Password">
                            <Field
                                type='password'
                                name='password'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                            />
                        </Input>
                    </fieldset>
                    {error ? <fieldset><p className="-attention">{error}</p></fieldset> : null}
                    <div>
                        <button type='submit' disabled={isSubmitting}>
                            Submit
                        </button>
                    </div>
                </form>
            )}
        </Formik>
    );
};
