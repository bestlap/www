import React, { useEffect, useCallback } from 'react';

export default function Modal ({ onClose, children }) {
    const close = () => {
        if (typeof onClose !== 'function') {
            return;
        }

        onClose();
    }

    const escape = useCallback((event) => {
        if (event.key !== "Escape") {
            return;
        }

        close();
      }, []);
    
      useEffect(() => {
        document.addEventListener("keydown", escape, false);
    
        return () => {
          document.removeEventListener("keydown", escape, false);
        };
      }, [escape]);

    return (
        <div className="modal">
            <div className="modal__overlay" onClick={close}></div>
            <div className="modal__control">
                <button type="button" className="modal__close" onClick={close}>X</button>
            </div>
            <div className="modal__content">
                {children}
            </div>
        </div>
    );
}