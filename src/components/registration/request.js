import React, { useState } from 'react';
import { Formik, Form } from 'formik';
// import { Persist } from '../../vendor/formik-persist';

import Input from '../form/input';
import Group from '../form/group';
import Field from '../form/field';
import Radio from '../form/radio';

import Class from '../form/class';
import Countries from '../form/countries';
import Agreement from '../form/agreement';

import { localize } from "../../content";

const CLASSES = [
    'Street A',
    'Street B',
    'Street C',
    'Unlim 2000',
    'Unlim 2000+',
];

const DEFAULTS = {
    name: '',
    email: '',
    instagram: '',
    country: 'Estonia',
    team: '',
    mark: '',
    model: '',
    year: '',
    engine: '',
    turbo: '',
    power: '',
    type: '',
    tyres: '',
    class: CLASSES[0] || '',
    time: '',
    copilot: '',
    agreement: '',
};

const classify = (type, tyres, turbo, engine, power) => {
    const volume = turbo === "Yes" ? 1.7 * engine : engine;

    if (type === 'Track' || tyres === '<180') {
        if (volume <= 2000) {
            return "Unlim 2000";
        }
        
        else {
            return "Unlim 2000+";
        }
    }

    else {
        if (volume <= 2000 && power <= 120) {
            return 'Street A';
        }

        else if ((volume <= 2000 && power > 120) || (volume > 2000 && volume <= 3000)) {
            return "Street B";
        }
        
        else {
            return "Street C";
        }
    }
};

const getRejected = classified => {
    const index = CLASSES.indexOf(classified);
    
    if (index < 0) {
        return [];
    }

    return [...CLASSES].slice(0, index);
}

const validateEmail = email => /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(email).toLowerCase());

const validate = values => {   
    const errors = {};

    for (const [key, value] of Object.entries(values)) {
        switch (key) {
            case 'name':
            case 'country':
            case 'mark':
            case 'model':
            case 'turbo':
            case 'type':
            case 'tyres':
            case 'agreement':
                if (value.length <= 0) {
                    errors[key] = key;
                }

                break;

            case 'email':
                if (!validateEmail(value)) {
                    errors[key] = key;
                }

                break;

            case 'year':
                if (!(!isNaN(value) && value >= 1885 && value <= new Date().getFullYear() + 1)) {
                    errors[key] = key;
                }

                break;

            case 'engine':
                if (!(!isNaN(value) && value > 50 && value <= 27000)) {
                    errors[key] = key;
                }

                break;

            case 'power':
                if (!(!isNaN(value) && value > 0 && value <= 746)) {
                    errors[key] = key;
                }

                break;

            case 'instagram':
            case 'team':
            case 'time':
            case 'copilot':

                break;

            default:

                break;
        }
    }

    return errors;
};

export default function RegistrationRequest ({
    lang, submit, values, authorized
}) {
    const [changes] = useState(values);
    const [rejected, setRejected] = useState([]);

    const changed = (form, setFieldValue, name) => {
        const classified = classify(form.type, form.tyres, form.turbo, form.engine, form.power);
        
        if (name !== 'class') {
            setFieldValue('class', classified);
        }
        
        setRejected(getRejected(classified));
    
    };

    return (
        <Formik
            initialValues={values ? {...DEFAULTS, ...changes} : DEFAULTS}
            validate={validate}
            onSubmit={submit}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                setFieldValue,
                /* and other goodies */
            }) => (
                <Form onSubmit={handleSubmit}>
                    <fieldset>
                        <legend>{localize('registration-participant', lang)}</legend>

                        <Input name="name" label={localize('registration-name', lang)}>
                            <Field
                                name="name"
                                type="text"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.name}
                                readOnly={authorized}
                            />
                        </Input>

                        <Input name="email" label={localize('registration-email', lang)}>
                            <Field
                                type='email'
                                name='email'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                                readOnly={authorized}
                            />
                        </Input>

                        <Input name="instagram" label={localize('registration-instagram', lang)}>
                            <Field
                                type='text'
                                name='instagram'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.instagram}
                            />
                        </Input>

                        <Input name="country" label={localize('registration-country', lang)}>
                            <Countries
                                name='country'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.country}
                            />
                        </Input>

                        <Input name="team" label={localize('registration-team', lang)}>
                            <Field
                                type='text'
                                name='team'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.team}
                            />
                        </Input>
                    </fieldset>

                    <fieldset>
                        <legend>{localize('registration-car', lang)}</legend>


                        <Input name="mark" label={localize('registration-mark', lang)}>
                            <Field
                                type='text'
                                name='mark'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.mark}
                            />
                        </Input>
                        

                        <Input name="model" label={localize('registration-model', lang)}>
                            <Field
                                type='text'
                                name='model'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.model}
                            />
                        </Input>

                        <Input name="year" label={localize('registration-year', lang)}>
                            <Field
                                name='year'
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.year}
                            />
                        </Input>  

                        <Input name="engine" label={localize('registration-engine', lang)}>
                            <Field
                                name='engine'
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.engine}
                                units={<>cm<sup>3</sup></>}
                                changed={changed}
                                setFieldValue={setFieldValue}
                            />
                        </Input>  

                        <Input name="turbo" label={localize('registration-turbo', lang)}>
                            <Group name="turbo">
                                <Radio
                                    id="turbo-yes"
                                    name='turbo'
                                    label={localize('yes', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value="Yes"
                                    checked={values.turbo === 'Yes' || values.turbo === '1'}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                                <Radio
                                    id="turbo-no"
                                    name='turbo'
                                    label={localize('no', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value="No"
                                    checked={values.turbo === 'No' || values.turbo === '0'}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                            </Group>
                        </Input> 

                        <Input name="power" label={localize('registration-power', lang)}>
                            <Field
                                name='power'
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.power}
                                units="kW"
                                changed={changed}
                                setFieldValue={setFieldValue}
                            />
                        </Input> 

                        <Input name="type" label={localize('registration-type', lang)}>
                            <Group name="type">
                                <Radio
                                    id="type-stock"
                                    name='type'
                                    value="Stock"
                                    checked={values.type === 'Stock'}
                                    label={localize('registration-type-stock', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                                <Radio
                                    id="type-track"
                                    name='type'
                                    value="Track"
                                    checked={values.type === 'Track'}
                                    label={localize('registration-type-track', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                            </Group>
                        </Input> 

                        <Input name="tyres" label={localize('registration-tyres', lang)}>
                            <Group name="tyres">
                                <Radio
                                    id="tyres-racing"
                                    name='tyres'
                                    value="<180"
                                    checked={values.tyres === '<180'}
                                    label={localize('registration-tyres-racing', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                                <Radio
                                    id="tyres-regular"
                                    name='tyres'
                                    value="≥180"
                                    checked={values.tyres === '≥180'}
                                    label={localize('registration-tyres-regular', lang)}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    changed={changed}
                                    setFieldValue={setFieldValue}
                                />
                            </Group>
                        </Input>

                        <Class
                            name='class'
                            label={localize('registration-class', lang)}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.class}
                            rejected={rejected}
                            setFieldValue={setFieldValue}
                            connected={changed}
                        />

                        <Input name="time" label={localize('registration-time', lang)} hint={localize('registration-time-hint', lang)}>
                            <Field
                                type='text'
                                name='time'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.time}
                            />
                        </Input>

                        <Input name="copilot" label={localize('registration-copilot', lang)} hint={localize('registration-copilot-hint', lang)}>
                            <Field
                                type='text'
                                name='copilot'
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.copilot}
                            />
                        </Input>
                    </fieldset>
                    
                    <Input name="agreement">
                        <Agreement
                            name='agreement'
                            label={localize('registration-agreement', lang)}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value="Yes"
                        />
                    </Input>

                    <div>
                        <button type="submit">
                            {localize('registration-submit', lang)}
                        </button>
                    </div>
                    {/** /}
                    <Persist name="request" isSessionStorage={true} ignore={['agreement']} />
                    {/ **/}
                </Form>
            )}  
        </Formik>
    );
};
