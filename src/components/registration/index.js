import React, { useEffect, useState } from 'react';
import { navigate } from 'gatsby'
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw'

import { authorize, get, logout, register, renew } from "../../services/api";

import Request from './request';

const REDIRECT = '/';

const STAGES = {
    'initialization': 0,
    'authorization': 1,
    'loading' : 2,
    'input': 3,
    'success': 4,
};

const sessionStorage = typeof window !== 'undefined' ? window.sessionStorage : {
    getItem: () => null,
    setItem: () => null,
    removeItem: () => null,
};

export default function Registration ({
    lang,
}) {
    const [stage, setStage] =  useState(STAGES.initialization);
    const [authorized, setAuthorized] = useState(false);
    const [profile, setProfile] = useState(null);
    const [success, setSuccess] = useState(sessionStorage.getItem('success'));

    const submit = (values, { setSubmitting, resetForm, setFieldError }) => {
        (authorized ? renew : register)(values, lang).then((response) => {
            if (response.status !== 'accepted') {
                setFieldError(response.reason, 'Invalid data');
                console.error(response.reason);

                return;
            }
            sessionStorage.setItem('success', response.message);
            setSuccess(response.message);
            setSubmitting(false);
            resetForm();
            localStorage.removeItem('request');
            setStage(STAGES.success);
        }).catch((rejection) => {
            setFieldError('agreement', rejection);
            console.warn(rejection);
        });
    }

    const logoff = () => {
        logout().then(() => {
            sessionStorage.removeItem('success');
            setAuthorized(false);
            setStage(STAGES.initialization);
            navigate(REDIRECT);
        }).catch((rejection) => {
            console.warn(rejection.reason);
        });
    };

    const stages = {
        [STAGES.initialization]: () => (
            <>
                Initializating...
            </>
        ),
        [STAGES.authorization]: () => (
            <>
                Authorizing...
            </>
        ),
        [STAGES.loading]: () => (
            <>
                Authorized! Loading data...
            </>
        ),
        [STAGES.input]: () => (
            <>
                <Request authorized={authorized} submit={submit} lang={lang} values={profile} />
            </>
        ),
        [STAGES.success]: () => (
            <>
                <ReactMarkdown rehypePlugins={[rehypeRaw]} children={success} />
                <br />
                <br />
                <br />
                <div>
                    <button type="button" onClick={event => logoff()}>Logout</button>
                </div>
            </>
        ),
    };

    useEffect(() => {
        if (stage === STAGES.success) {
            return;
        }

        if (success) {
            setStage(STAGES.success);

            return;
        }

        setStage(STAGES.authorization);

        try {
            authorize().then(response => {
                if (response.status !== 'accepted') {
                    console.warn(response.reason);
                    setStage(STAGES.input);

                    return;
                }

                setAuthorized(true);
            }).catch(rejection => {
                console.warn(rejection.reason);
            });
        }
        
        catch (rejection) {
            console.warn(rejection.reason);
        }
    }, []);

    useEffect(() => {
        if (stage === STAGES.success) {
            return;
        }

        if (!authorized) {
            return;
        }

        try {
            setStage(STAGES.loading);

            get('profile').then(response => {
                if (response.status !== 'accepted') {
                    console.warn(response.reason, response);

                    return;
                }

                if (!response.profile) {
                    console.warn('No profile data');

                    return;
                }
                
                setStage(STAGES.input);
                setProfile(response.profile);
                
            }).catch((rejection) => {
                console.warn(rejection.reason);
            });
        }
        
        catch (rejection) {
            console.warn(rejection.reason);
        }
    }, [authorized]);

    return stages[stage]();
}
