import React from 'react';
import Index from '.';
import { localize } from "../content";

const columns = ['number', 'name', 'mark', 'model', 'engine', 'turbo', 'type', 'tyres'];

export const mark = name => {
    let mark = name.toLowerCase().replace(' ', '-');

    switch (mark) {
        case 'vw':
            mark = 'volkswagen';
            break;
        default:
            break;
    }

    return mark;
}


export default class Pilots extends React.Component {
    state = {
        pilots: null,
    };

    componentDidMount() {
        this._asyncRequest = renderPilots(this.props).then(
        pilots => {
            this._asyncRequest = null;
            this.setState({pilots});
        }
        );
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    render() {
        return this.state.pilots === null ? (<div className="loading"></div>) : (<>{this.state.pilots}</>);
    }
}

function Class (props) {
    return (<div className="class">
        <h3>{props.name}</h3>
        {props.children}
    </div>);
}

function Pilot (props) {
    const classList = ['pilot'];

    if (props.data.car.color !== '') {
        classList.push('-' + props.data.car.color);
    }

    if (props.data.paid === '1') {
        classList.push('-paid');
    }

    return (<div className={classList.join(' ')}>
        {columns.map(column => (<div className={column} key={column}>{renderPilotColumn(column, props.data)}</div>))}
    </div>);
}

function renderPilotColumn(column, pilot) {
    switch (column) {
        case 'name':
            return pilot.driver;
        case 'mark':
            return <img src={'/logos/' + mark(pilot.car.mark) + '.png'} alt={pilot.car.mark} />;
        case 'type':
            return pilot.car[column] === 'Stock' ? 'Street' : pilot.car[column];
        case 'model':
        case 'year':
            return pilot.car[column];
        case 'engine':
            return (pilot.car.engine.volume / 1000).toFixed(1);
        case 'turbo':
            return pilot.car.engine.turbo ? 'T' : '';
        case 'tyres':
            return pilot.car.wheels[column]
        default:
            return pilot[column];
    }
}

async function renderPilots (props) {
    const random = '?' + (Math.random() + 1).toString(36).substring(7);
    const race = await fetch('/race.json' + random).then(response => response.json());
    const racers = await fetch('/racers.json' + random).then(response => response.json());
    const classed = await fetch('/classes.json' + random).then(response => response.json());
    const cars = await fetch('/cars.json' + random).then(response => response.json());
    const statuses = await fetch('/statuses.json' + random).then(response => response.json());
    const index = Index(race, racers, classed, cars, statuses);
    const out = [];

    for (let classen in index.classes) {
        if (index.classes[classen].length <= 0) {
            continue;
        };

        out.push(
            <Class name={classen} key={classen}>
                {index.classes[classen].map(pilot => (<Pilot data={pilot} key={pilot.number} />))}
            </Class>
        );
    }

    const reduced = Object.values(index.classes).reduce((acc, curr) => acc.concat(curr), []);

    return (
        <div className="pilots">
            <p>
                {localize('paid', props.lang)}: {reduced.filter(pilot => pilot.paid).length}/40
                <br />
                {localize('total', props.lang)}: {reduced.filter(pilot => pilot).length}
            </p>
            {out.map(classen => classen)}
        </div>
    );
}
