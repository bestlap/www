import { useState, useEffect, useRef } from "react";
import debounce from "lodash.debounce";

export default function useInput () {
    const [keys, setKeys] = useState(false);
    const [scroll, setScroll] = useState(0);
  	const [lastScrollY, setLastScrollY] = useState(0);
	
	const [scrollPosition, setScrollPosition] = useState(0);

	const debouncedSetScrollPosition = useRef(debounce((scrollY) => {
		setScrollPosition(scrollY);

	}, 100)).current;

	const handleScroll = () => debouncedSetScrollPosition(window.scrollY);

	useEffect(() => {
		const keify = () => {
			setKeys(true);

			window.removeEventListener('keydown', keify, false);
		};
		
		window.addEventListener('keydown', keify, false);
	}, []);

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);

		return () => {
			window.removeEventListener('scroll', handleScroll);
			
			debouncedSetScrollPosition.cancel();
		};
	}, [debouncedSetScrollPosition]);


	useEffect(() => {
		if ((scrollPosition - lastScrollY > 2) && scrollPosition > 80) {
			setScroll(1); 
		}

		if (scrollPosition === 0) {
			setScroll(0);
		}

		if (lastScrollY - scrollPosition > 2) {
			setScroll(-1);  
		}

		setLastScrollY(scrollPosition);
	}, [scrollPosition]);

    return [keys, scroll];
}
