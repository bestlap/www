import React, { useEffect } from 'react';
import { localize } from "../content";

const capitalize = name => name.split(' ').map((word, index) => {
    switch (word.toLowerCase()) {
        case 'a':
            
        case 'at':
        case 'but':
        case 'for':
        case 'in':
        case 'or':
        case 'of':
        case 'per':
        case 'the':

            return index === 0 ? word.toLowerCase().charAt(0).toUpperCase() + word.toLowerCase().slice(1) : word.toLowerCase();
        
        default:
            
            return word.toLowerCase().charAt(0).toUpperCase() + word.toLowerCase().slice(1);
    }
}).join(' ');

const merge = array => array.reduce((previous, current) => [...previous, ...current]);

export default class ResultsTable extends React.Component {
    state = {
        calculated: {
            pilots: null,
            points: null,
            results: {},
            classes: null,
            teams: {},
        },
    };

    constructor (props) {
        super(props);
        
        this.dates = props.dates;
        this.lang = props.lang

        this.headings = ['name', 'car', ...this.dates.map((date, index) => index + 1), 'total'];
    
        this.columns = [
            'points',
            'cars',
            'racers',
            'teams',
        ];
    
        this.reference = React.createRef();
      }

    componentDidMount() {
        this._asyncRequest = renderResults(this.dates, this.columns).then(
            calculated => {
                this._asyncRequest = null;

                this.setState({ calculated });
            }
        );
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    render() {
        const { pilots, points , results, classes, teams } = this.state.calculated;

        return pilots === null ? (<div className="loading"></div>) : <Table lang={this.lang} reference={this.reference} pilots={pilots} points={points} results={results} classes={classes} teams={teams} dates={this.dates} columns={this.columns} headings={this.headings} />;
    }
}

async function renderResults (dates, columns) {
    const state = await columnize(dates, columns);
    const pilots = new Pilots(state.racers);
    const points = new Points(state.points);
    const teams = new Teams(state.teams, points);
    const results = new Results(pilots, points, state.cars);
    const classes = Object.keys(results);

    return { pilots, points, results, classes, teams };
}


async function columnize (dates, columns) {
    const columnized = {};

    await Promise.all(columns.map(async column => {
        columnized[column] = [];

        await Promise.all(dates.map(async date => columnized[column].push(await fetch('/time/' + date + '/' + column + '.json').then(response => response.json()))));
    }));

    return columnized;
}

function Table ({ lang, reference, pilots, points , results, classes, teams, dates, columns, headings }) {
    const out = [];
    const teamed = Object.keys(teams);

    if (teamed.length > 0) {
        out.push({
            name: localize('results-team', lang),
            units: teamed.filter(name => 'total' in teams[name]).map((name, index) => <Team name={name} place={index + 1} data={teams[name]} key={index} dates={dates} />),
        })
    };

    for (let classen in results) {
        const classed = Object.keys(results[classen]);

        if (classed.length <= 0) {
            continue;
        };

        out.push({
            name: classen,
            units: classed.map((pilot, index) => (<Pilot name={pilot} place={index + 1} data={results[classen][pilot]} key={index} dates={dates} />)),
        })
    }

    const click = event => {
        var head = event.target;
        var row = Array.prototype.slice.call(reference.current.tHead.rows[0].children);
        var idx = row.indexOf(head);

        sort(reference.current, headings, idx);
    };

    useEffect(() => {
        sort(reference.current, headings, headings.indexOf('total'), false);
        placefy(reference.current);

    }, [reference]);

    return (
        <table className="results" ref={reference}>
            <thead>
                <tr>
                    <th className="name" onClick={click}>{localize('results-name', lang)}</th>
                    <th className="car" onClick={click}>{localize('results-car', lang)}</th>
                    {dates.map((date, index) => (
                        <th key={index + 1} data-sort-default="desc" className={'round ' + (index + 1)} onClick={click}>{romify(index + 1)}</th>
                    ))}
                    <th data-sort-default="desc" data-sort="desc" className="total" onClick={click}></th>
                </tr>
            </thead>
            {out.map((group, index) => <Group name={group.name} key={index} dates={dates} headings={headings}>{group.units}</Group>)}
        </table>
    );
}

function Group (props) {
    return (
    <>
        <thead>
            <tr>
                <th colSpan={props.headings.length}>
                    {props.name}
                </th>
            </tr>
        </thead>
        <tbody>
            {props.children}
        </tbody>
    </>
    );
}

function Pilot (props) {
    const classList = ['pilot'];
    var numbers = [];

    for (let number in props.data.cars) {
        numbers.push(number);
    }

    const car = props.data.cars[numbers[numbers.length - 1]];
    const marked = '/logos/' + mark(car.mark) + '.png';

    return (
    <tr data-place={props.place} className={classList.join(' ')}>
        <td className="name">
            <div className="sequence">
            <span>{props.name}</span>
            <div className="numbers">
            {numbers.map((number, index) => <figure className="number" key={index} id={number}><figcaption>{number}</figcaption></figure>)}
            </div>
            </div>
        </td>
        <td className="car">
            <figure className="mark">
                <figcaption>
                    <span className="mark">{car.mark}</span>
                    <span className="model">{car.model}</span>
                </figcaption>
                <img src={marked} />
            </figure>
        </td>
        {props.dates.map((date, index) => (
            <td className={'round ' + (index + 1)}  key={index + 1}>{props.data[index]}</td>
        ))}
        <td className="total">{props.data.total}</td>
    </tr>
    );
}


function Pilots (racers) {
    var Pilots = {};

    racers.forEach(function (racer, index) {
        for (let number in racer) {
            if (isNaN(number)) {
                continue;
            }
            var name = racer[number];

            if (!exists(name)) {
                Pilots[name] = [];
            }
            if (Pilots[name].indexOf(parseInt(number)) > -1) {
                continue;
            }
            Pilots[name].push(parseInt(number));
        }
    });

    function exists (name) {
        var exists = false;

        for (let pilot in Pilots) {
            // console.log('name', name, pilot)
            if (name.toLowerCase() === pilot.toLowerCase()) {
                if (name !== pilot) {
                    console.log('%c ! ', 'background: red', pilot, name)
                    Object.defineProperty(Pilots, name,
                        Object.getOwnPropertyDescriptor(Pilots, pilot));
                    delete Pilots[pilot];
                }
                exists = true;
                break;
            }
        }

        return exists;
    }
    
    return Pilots;
}

function Points (races) {
    var Points = {};

    races.forEach(function (race, index) {
        for (let classen in race) {
            if (!(classen in Points)) {
                Points[classen] = {};
            }

            for (let result in race[classen]) {
                var number = race[classen][result][0];
                var points = race[classen][result][1];

                if (!(number in Points[classen])) {
                    Points[classen][number] = {};
                }

                Points[classen][number][index] = points;
            }
        }
    });

    return Points;
}

function Team (props) {
    const classList = ['team'];

    return (
    <tr data-place={props.place} className={classList.join(' ')}>
        <td className="name" colSpan="2">
            <div className="sequence">
            <span>{props.name}</span>
            <div className="numbers">
            {props.data.numbers.map((number, index) => <a href={'#' + number} key={index}><figure className="number"><figcaption>{number}</figcaption></figure></a>)}
            </div>
            </div>
        </td>
        {props.dates.map((date, index) => (
            <td className={'round ' + (index + 1)}  key={index + 1}>{props.data[index]}</td>
        ))}
        <td className="total">{props.data.total}</td>
    </tr>
    );
}

function Teams (teams, points) {
    const Teams = {};
    
    const reduced = teams.map(round => Object.keys(round).filter(name => name !== "").map(key => capitalize(key)).filter((item, pos, self) => self.indexOf(item) === pos).sort()).sort();

    reduced.map(round => round.map(name => Teams[capitalize(name)] = {}));

    teams.map((round, roundIndnext) => Object.keys(round).filter(name => name !== "").map((name, teamIndex) => {
        if (!('numbers' in Teams[capitalize(name)])) {
            Teams[capitalize(name)].numbers = [];
        }

        Teams[capitalize(name)].numbers = [...new Set([...Teams[capitalize(name)].numbers, ...round[name]])];
    }));

    Object.keys(points).map(classified => Object.keys(points[classified]).map(number => {
        Object.keys(points[classified][number]).map((round, roundIndex) => {
            const team = reduced[parseInt(round)].find(name => Teams[capitalize(name)].numbers.includes(number));

            if (!team) {
                return;
            }

            if (!(round in Teams[team])) {
                Teams[team][round] = 0;
            }

            if (!('total' in Teams[team])) {
                Teams[team].total = 0;
            }

            Teams[team][round] += points[classified][number][round] || 0;
            Teams[team].total += points[classified][number][round] || 0;
        });        
    }));

    return Teams;
}

function Results (pilots, points, cars) {
    var Results = {};

    for (let classen in points) {
        Results[classen] = {};

        for (let pilot in pilots) {
            var total = [];

            if (!(pilot in Results[classen])) {
                
            }
            pilots[pilot].forEach(function (number) {
                if (!(number in points[classen])) {
                    return;
                }
                if (!(pilot in Results[classen])) {
                    Results[classen][pilot] = {};
                }
                for (let stage in points[classen][number]) {
                    if (!('cars' in Results[classen][pilot])) {
                        Results[classen][pilot].cars = {};
                    }
                    total.push(points[classen][number][stage]);

                    Results[classen][pilot][stage] = points[classen][number][stage];
                    Results[classen][pilot].cars[number] = getCar(number);
                }
                Results[classen][pilot].total = total.length > 4 ? total.reduce((a, b) => a + b, 0) - total.find(a => a === Math.min(...total)): total.reduce((a, b) => a + b, 0);
            });
        }
    }

    function getCar (number) {
        var car = undefined;

        cars.forEach(function (round) {
            if (!("length" in round)) {
                return;
            }

            round.forEach(function(c) {
                if (typeof car !== 'undefined') {
                    return;
                }
                c.number.forEach(function (n) {
                    if (typeof car !== 'undefined') {
                        return;
                    }
                    if (n === number) {
                        car = c;
                    }
                });
            })
        });

        return car;
    }

    return Results;
}


function sort (table, headings, idx, asc) {
    const ths = table.tHead.rows[0].children;
    const th = ths[idx];

    if (typeof asc === 'boolean') {
        th.asc = asc;
    } else if ('sort' in th.dataset) {
        th.asc = th.dataset.sort === 'asc';
        th.asc = !th.asc;
    } else if ('sortDefault' in th.dataset) {
        th.asc = th.dataset.sortDefault === 'asc';
    /*
    } else if ('asc' in th && typeof th.asc === 'boolean') {
        console.log('4');
        th.asc = !th.asc;
    */
    } else {
        th.asc = true;
    }

    Array.prototype.forEach.call(ths, function (th) {
        delete th.dataset.sort;
    })

    th.dataset.sort = th.asc ? 'asc' : 'desc';

    Array.prototype.forEach.call(table.tBodies, function (tbody) {
        Array.prototype.slice.call(
            tbody.querySelectorAll('tr')).sort(comparer(headings, idx, th.asc)).forEach(function(tr) {
            tbody.appendChild(tr)
        });
    });
}


function comparer (headings, idx, asc) {
    console.log('comparer reporting idx, asc', idx, asc, headings[idx])
    return function (a, b) {
        return headings[idx] === 'total' ? total(asc, a, b, idx) : (function (v1, v2) {
            return (
                v1 !== '' &&
                v2 !== '' &&
                !isNaN(v1) &&
                !isNaN(v2) ?
                    v1 - v2:
                    v1.toString().localeCompare(v2)
            );
        } (
            getCellValue(asc ? a : b, idx),
            getCellValue(asc ? b : a, idx)
        ));
    };
};

function total (asc, a, b, idx) {
    var v1 = getCellValue(asc ? a : b, idx);
    var v2 = getCellValue(asc ? b : a, idx);

    return v1 - v2 === 0 ? staged(asc, a, b, idx) : v1 - v2;
}

function staged (asc, a, b, idx) {
    var result = 0;
    var stages = stagefy(asc, a, b, idx);

    var max = maxStages(stages);
    var counted = countSameScore(stages, max.a);

    console.log('stages, counted and max', stages, counted, max);
    var iterated = generatePlacesMatrix(stages);
    var placed = countPlaces(iterated);
    var first = bestFirst(stages);

    if (max.a === max.b) {
        if (placed.a === placed.b) {
            placed = asc ? placed.b - placed.a : placed.a - placed.b;
        }
    } else {
        result = asc ? max.b - max.a : max.a - max.b;
    }

    return max.a !== max.b ?
    (asc ? max.b - max.a : max.a - max.b) : (
        placed.a !== placed.b ?
        (asc ? placed.b - placed.a : placed.a - placed.b) :(
            first.a !== first.b ?
            (asc ? first.b - first.a : first.a - first.b) :
            0
        )
    );
}

function stagefy (asc, a, b, idx) {
    var stages = {
        a: [],
        b: []
    };
    var i = 1;

    while (!isNaN(getCellValue(asc ? a : b, idx - i))) {
        stages.a.push(getCellValue(asc ? a : b, idx - i));
        stages.b.push(getCellValue(asc ? b : a, idx - i));
        
        i++;
    }

    return stages;
}

function generatePlacesMatrix (stages) {
    const points = [10, 8, 6, 5, 4, 3, 2, 1];
    const iterated = {
        a: [],
        b: []
    }

    for (let v in iterated) {
        points.forEach(function () {
            iterated[v].push(0);
        });
    }

    for (let v in stages) {
        stages[v].forEach(function (score, index) {
            if (parseInt(score) === 0) {
                return;
            }
            const place = points.indexOf(parseInt(score));

            iterated[v][place]++;
        });
    }

    return iterated;
}

function countPlaces (iterated) {
    const counted = {
        a: 0,
        b: 0
    };

    iterated.a.forEach(function (times, place) {
        if (counted.a !== counted.b) {
            return;
        }
        if (times === iterated.b[place]) {
            return;
        }
        if (iterated.b[place] > times) {
            counted.b++;
        } else {
            counted.a++;
        }
    });

    return counted;
}

function bestFirst (stages) {
    const first = {
        a: stages.a.reverse(),
        b: stages.b.reverse()
    }

    return bestLast(first);
}

function bestLast (stages) {
    const last = {
        a: 0,
        b: 0
    }

    stages.a.forEach(function (points, index) {
        if (last.a !== last.b) {
            return;
        }
        if (points === stages.b[index]) {
            return;
        }
        if (points > stages.b[index]) {
            last.a++;
        } else {
            last.b++;
        }
    });

    return last;
}

function maxStages (stages) {
    const max = {
        a: 0,
        b: 0
    };

    for (let v in stages) {
        stages[v].forEach(function (score, index) {
            if (score > max[v]) {
                max[v] = score;
            }
        });
    }

    return max;
}

function countSameScore (stages, scored) {
    const counted = {
        a: 0,
        b: 0
    }
    
    for (let v in stages) {
        stages[v].forEach(function (score, index) {
            if (score === scored) {
                counted[v]++;
            }
        });
    }

    return counted;
}

function getCellValue (tr, idx) {
    const id = Array.from(tr.children).filter((td, index) => idx >= index).map(td => td.colSpan).length - 1;
    
    return tr.children[id].innerText || tr.children[id].textContent;
}

function placefy (table) {
    Array.prototype.forEach.call(table.tBodies, function (tbody) {
        Array.prototype.forEach.call(tbody.rows, function (tr, idx) {
            tr.dataset.place = idx + 1;
        });
    });
}


function romify (num) {
    if (typeof num !== 'number') {
        return false;
    }
    var digits = String(+num).split("");
    var key = [
        "","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
        "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
        "","I","II","III","IV","V","VI","VII","VIII","IX"
    ];
    var roman_num = "",
    i = 3;
    while (i--) roman_num = (key[+digits.pop() + (i * 10)] || "") + roman_num;

    return Array(+digits.join("") + 1).join("M") + roman_num;
}

function mark (name) {
    var mark = name.toLowerCase().replace(' ', '-');

    switch (mark) {
        case 'vw':
            mark = 'volkswagen';
            break;
    }

    return mark;
}
