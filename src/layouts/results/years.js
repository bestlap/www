import React from 'react';
import { Link } from "gatsby"

import { getPathnamePure, getPathnameForPure } from '../_helpers';

import languages from "../../content/languages";

export default function Years (props) {
    const lang = props.pageContext.langKey;
    const years = props.years.reverse();
    const path = props.path;

    const getPathname = pathname => getPathnamePure(pathname, lang, languages);
    const getPathnameFor = locale => getPathnameForPure(locale, path, lang, languages);

    return (
        <nav className="tabs">
            <ul>
                {years.map(year => (
                    <li key={year}>
                        <Link to={year === years[0] && (getPathnameFor(languages.default).indexOf(`/results/${years[0]}/`) === 0 || getPathnameFor(languages.default) === '/results/') ? getPathname('results') : getPathname(`results/${year}`)} activeClassName="-active" partiallyActive={true}>
                            {year}
                        </Link>
                    </li>
                ))}
            </ul>
        </nav>
    );
}
