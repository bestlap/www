import React from 'react';
import { Link } from "gatsby"

import { getPathnamePure } from '../_helpers';

import languages from "../../content/languages";

export default function Dates (props) {
    const lang = props.pageContext.langKey;
	const { year, dates } = props;

    const getPathname = pathname => getPathnamePure(pathname, lang, languages);
    // const getPathnameFor = locale => getPathnameForPure(locale, path, lang, languages);

    return (
        <nav className="tabs -dates">
            <ol type="I">
                {Object.keys(dates).reverse().map(date => (
                    <li key={year + date}>
                        <Link to={getPathname(`results/${year}/${date}`)} activeClassName="-active" partiallyActive={true}>
                            {dates[date]}
                        </Link>
                    </li>
                ))}
          </ol>
        </nav>
    );
}
