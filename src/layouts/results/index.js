import React, { useState } from 'react';
import { Helmet } from 'react-helmet';

import Years from '../../layouts/results/years';
import Dates from '../../layouts/results/dates';
import ResultsTable from '../../components/results';
import Time from '../../components/time/static';
import Modal from "../../components/modal"
import Laps from "../../components/time/laps"

import { localize } from '../../content';

import events from '../../content/events.json'

export default function Results (props) {
    const { year, date, index } = props;
    const [modal, setModal] = useState(null);
    const click = laps => setModal(<Laps laps={laps} />);
    const close = () => setModal(null);

    return (
        <>
            <Helmet>
                <title>{localize('results', props.pageContext.langKey)}</title>
            </Helmet>

            <h1>{localize('results', props.pageContext.langKey)} <span className="-hide">{year}</span></h1>
            <section className="headings">
                <article>
                    <Years years={Object.keys(events)} {...props} />
                    <Dates year={year} dates={events[year]} {...props}/>
                </article>
            </section>
            
            {date ? (
                <>
                    <section className="time">
                        <Time lang={props.lang} index={index} onClick={click} />
                    </section>
                    {modal && <Modal onClose={close}>{modal}</Modal>}
                </>
            ) : (
                <section className="results">
                    <ResultsTable dates={Object.keys(events[year]).reverse().map(date => `${year}-${date}`)} lang={props.pageContext.langKey} />
                </section>
            )}
        </>
    );
}