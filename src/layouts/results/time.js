import React from 'react';

import Years from '../../layouts/results/years';
import Dates from '../../layouts/results/dates';
import ResultsTable from '../../components/results';
import Static from '../../../../components/time/static'

import { localize } from '../../content';

import events from '../../content/events.json'

export default function Time (props) {
    const { year, index } = props;
    const date = undefined;

    console.log('events', events);
    console.log('events[year]', events[year])

    return (
        <section className="results">
            <article>
                <h2>{localize('results', props.pageContext.langKey)} <span className="-hide">{year}</span></h2>
                <Years years={Object.keys(events)} {...props} />
                <Dates year={year} dates={events[year]} {...props}/>
            </article>
            {date ? (
                <>DATE</> 
            ) : (
                <ResultsTable dates={Object.keys(events[year]).reverse().map(date => `${year}-${date}`)} lang={props.pageContext.langKey} />
            )}
      </section>
    );
}