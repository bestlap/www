import React, { useRef, useEffect } from "react";
import { Link } from "gatsby"
import { localize } from "../content";
import { Helmet } from "react-helmet";
import languages from "../content/languages";
import Supporters from "../components/supporters";
import Parallax from "../components/parallax";	
import Cookie from "../components/cookie";
import useInput from "../components/input";

import { getPathnamePure, getPathnameForPure } from './_helpers';

import '../style/index.scss';

const Layout = (props) => {
	const index = props.index;
    const lang = props.pageContext.langKey;
	const path = props.path;

    const getPathname = pathname => getPathnamePure(pathname, lang, languages);
    const getPathnameFor = locale => getPathnameForPure(locale, path, lang, languages);

	const [keys, scroll] = useInput();

	const everything = useRef(null);

	useEffect(() => {
		const className = 'everything' + (keys ?  ' -keys' : '') + (scroll > 0 ? ' -forward' : scroll < 0 ? ' -reverse' : '');

		everything.current.className = className;
	}, [keys, scroll]);

    return (<div className="everything" ref={everything}>
		<Helmet htmlAttributes={{
          lang: props.pageContext.langKey
		}}/>
		<a className="skip" href="#content">
			Skip to content
		</a>

        <header className="header">
			<figure className="logo">
            	<Link to={getPathname('/')} title="Start">
					<svg version="1.1" width="100px" height="100px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100">
						<g>
							<path fill="#C9C9CA" d="M50.001,0C22.39,0,0.002,22.386,0,50c0.002,27.615,22.39,50,50.001,50
								c27.616,0,50.001-22.385,49.999-49.997C100.002,22.383,77.62,0,50.001,0z"/>
							<path fill="#47B549" d="M89.62,50.003c0.001,21.89-17.746,39.64-39.641,39.64C28.082,89.643,10.333,71.89,10.333,50
								c0-21.897,17.747-39.645,39.642-39.643C71.872,10.355,89.62,28.107,89.62,50.003z"/>
							<path fill="#FFFFFF" d="M20.186,32.537h7.043c1.73,0,3.094,0.479,3.958,1.348c0.697,0.692,1.043,1.536,1.043,2.623
								c0,1.709-0.911,2.662-1.996,3.271c1.759,0.666,2.839,1.686,2.839,3.768c0,2.772-2.253,4.158-5.674,4.158h-7.213V32.537z
								M26.51,38.669c1.474,0,2.403-0.478,2.403-1.647c0-0.996-0.78-1.561-2.188-1.561h-3.291v3.208H26.51z M27.398,44.776
								c1.471,0,2.359-0.521,2.359-1.69c0-1.015-0.755-1.644-2.467-1.644h-3.856v3.334H27.398z"/>
							<path fill="#FFFFFF" d="M37.296,32.537H48.73v2.97h-8.125v3.076h7.151v2.969h-7.151v3.183h8.234v2.969H37.296V32.537z"/>
							<path fill="#FFFFFF" d="M52.241,45.495l1.968-2.365c1.366,1.126,2.796,1.842,4.527,1.842c1.364,0,2.19-0.541,2.19-1.471
								c0-0.844-0.52-1.277-3.055-1.93c-3.055-0.783-5.028-1.622-5.028-4.675c0-2.751,2.212-4.57,5.312-4.57
								c2.207,0,4.092,0.69,5.631,1.921l-1.734,2.516c-1.343-0.932-2.664-1.497-3.943-1.497c-1.278,0-1.947,0.585-1.947,1.366
								c0,1,0.648,1.32,3.271,1.994c3.072,0.798,4.81,1.905,4.81,4.592c0,3.014-2.299,4.698-5.568,4.698
								C56.374,47.916,54.06,47.115,52.241,45.495z"/>
							<path fill="#FFFFFF" d="M71.817,35.614h-4.612v-3.077H79.77v3.077h-4.615v12.09h-3.338V35.614z"/>
							<path fill="#FFFFFF" d="M27.291,53.195h3.334v12.132h7.557v3.032H27.291V53.195z"/>
							<path fill="#FFFFFF" d="M47.366,53.086h3.077l6.497,15.272h-3.488l-1.384-3.407h-6.414l-1.387,3.407H40.87L47.366,53.086z
								M50.877,62.009l-2.015-4.912l-2.017,4.912H50.877z"/>
							<path fill="#FFFFFF" d="M60.749,53.195h6.199c3.615,0,5.803,2.146,5.803,5.287c0,3.509-2.729,5.326-6.133,5.326h-2.533v4.551
								h-3.336V53.195z M66.729,60.837c1.666,0,2.642-0.989,2.642-2.336c0-1.49-1.04-2.294-2.707-2.294h-2.579v4.63H66.729z"/>
							<path fill="#FFFFFF" d="M89.132,22.978c-0.592,0.599-1.187,1.198-1.781,1.801c-0.563-0.775-1.149-1.537-1.751-2.279
								c0.597-0.602,1.189-1.202,1.786-1.802C87.989,21.441,88.572,22.203,89.132,22.978z"/>
							<path fill="#FFFFFF" d="M85.6,22.5c-0.596,0.597-1.188,1.199-1.783,1.799c-0.607-0.746-1.232-1.477-1.874-2.185
								c0.599-0.599,1.196-1.2,1.794-1.798C84.377,21.029,84.999,21.757,85.6,22.5z"/>
							<path fill="#FFFFFF" d="M81.943,22.114c-0.595,0.596-1.187,1.194-1.784,1.793c-0.646-0.712-1.306-1.408-1.983-2.087
								c0.597-0.595,1.193-1.197,1.792-1.793C80.644,20.705,81.305,21.398,81.943,22.114z"/>
							<path fill="#FFFFFF" d="M85.565,26.582c-0.592,0.603-1.183,1.206-1.779,1.806c-0.566-0.78-1.152-1.542-1.756-2.293
								c0.596-0.597,1.189-1.195,1.787-1.795C84.417,25.046,85.003,25.804,85.565,26.582z"/>
							<path fill="#FFFFFF" d="M85.528,18.516c-0.6,0.602-1.193,1.201-1.791,1.8c-0.639-0.712-1.303-1.409-1.98-2.087
								c0.602-0.594,1.199-1.189,1.799-1.786C84.229,17.117,84.884,17.81,85.528,18.516z"/>
							<path fill="#FFFFFF" d="M81.776,18.231c-0.596,0.602-1.195,1.2-1.789,1.798c-0.679-0.68-1.375-1.338-2.088-1.981
								c0.604-0.599,1.201-1.191,1.799-1.79C80.409,16.899,81.099,17.561,81.776,18.231z"/>
							<path fill="#FFFFFF" d="M77.866,18.047c-0.601,0.595-1.197,1.192-1.795,1.788c-0.715-0.644-1.447-1.269-2.197-1.87
								c0.604-0.596,1.207-1.188,1.805-1.79C76.427,16.783,77.152,17.403,77.866,18.047z"/>
							<path fill="#FFFFFF" d="M81.471,14.436c-0.599,0.594-1.197,1.191-1.795,1.787c-0.711-0.641-1.439-1.264-2.182-1.863
								c0.599-0.595,1.201-1.189,1.801-1.783C80.037,13.179,80.764,13.799,81.471,14.436z"/>
							<path fill="#FFFFFF" d="M77.495,14.359c-0.602,0.593-1.205,1.187-1.802,1.786c-0.743-0.605-1.508-1.19-2.286-1.752
								c0.604-0.592,1.207-1.191,1.806-1.783C75.99,13.175,76.751,13.758,77.495,14.359z"/>
							<path fill="#FFFFFF" d="M79.978,24.087c0.628,0.728,1.233,1.48,1.81,2.252c0.078-0.081,0.16-0.163,0.242-0.245
								c-0.605-0.743-1.23-1.475-1.871-2.187C80.097,23.967,80.039,24.027,79.978,24.087z"/>
							<path fill="#FFFFFF" d="M78.022,21.982c0.055-0.054,0.107-0.107,0.161-0.161c-0.677-0.678-1.373-1.343-2.087-1.983
								c-0.061,0.06-0.121,0.121-0.184,0.181C76.641,20.648,77.342,21.302,78.022,21.982z"/>
							<path fill="#FFFFFF" d="M73.654,18.203c0.079-0.08,0.161-0.158,0.241-0.238c-0.746-0.609-1.51-1.188-2.286-1.757
								c-0.116,0.114-0.231,0.229-0.347,0.344C72.08,17.073,72.877,17.624,73.654,18.203z"/>
							<path fill="#FFFFFF" d="M81.083,10.834c-0.746-0.595-1.512-1.168-2.294-1.72c-0.589,0.58-1.179,1.16-1.767,1.738
								c0.773,0.567,1.531,1.152,2.277,1.752C79.89,12.015,80.487,11.426,81.083,10.834z"/>
							<path fill="#FFFFFF" d="M83.254,12.662c-0.594,0.591-1.188,1.179-1.783,1.774c0.712,0.64,1.405,1.293,2.082,1.969
								c0.594-0.594,1.188-1.186,1.783-1.782C84.661,13.95,83.966,13.296,83.254,12.662z"/>
							<path fill="#FFFFFF" d="M89.16,18.908c-0.379-0.475-0.765-0.942-1.159-1.404c-0.222-0.256-0.443-0.513-0.67-0.766
								c-0.006-0.005-0.011-0.011-0.017-0.018c-0.594,0.602-1.193,1.199-1.786,1.796c0.636,0.712,1.255,1.439,1.858,2.182
								C87.979,20.103,88.568,19.504,89.16,18.908z"/>
						</g>
						</svg>
						<figcaption>Best Lap</figcaption>
				</Link>
			</figure>
            <nav className="lang">
				<ul>
					<li>
						<Link to={getPathnameFor('et')} locale="et">
							Et
						</Link>
					</li>
					<li>
						<Link to={getPathnameFor('en')} locale="en">
							En
						</Link>
					</li>
					<li>
						<Link to={getPathnameFor('ru')} locale="ru">
							Ru
						</Link>
					</li>
				</ul>
            </nav>
			<nav className="menu">
			<ul>
{/*
				<li>
                    <a href="https://time.bestlap.ee/">
                        {localize('time', lang)}
                    </a>
                </li>

				<li>
                    <Link to={getPathname('registration')}>
                        {localize('registration', lang)}
                    </Link>
                </li>

				<li>
                    <Link to={getPathname('live')}>
                        {localize('live', lang)}
                    </Link>
                </li>
*/}
                <li>
                    <Link to={getPathname('regulations')}>
                        {localize('regulations', lang)}
                    </Link>
                </li>
{/*
				<li>
                    <Link to={getPathname('pilots')}>
                        {localize('racers', lang)}
                    </Link>
                </li>
*/}
				<li>
                    <Link to={getPathname('results')} activeClassName="-active" partiallyActive={true}>
                        {localize('results', lang)}
                    </Link>
                </li>
			</ul>
		</nav>
		</header>
		<Parallax />
		{index ? (
			<figure className="hero -index">
				<img
					srcSet="/media/2023/820x360-a.jpg 820w, /media/2023/820x360-a-2x.jpg 1640w"
					sizes="(max-width: 820px) 820px, 1640w"
					src="/media/2023/820x360-a.jpg"
					alt="Season 2023"
				/>
				<figcaption>
					<ul>
						<li className="-i"> <em>{localize('may', lang)} 13</em> <strong>Porschering</strong></li>
						<li className="-ii"><em>{localize('june', lang)}  10</em> <strong>Raassilla <span>RX</span></strong></li>
						<li className="-iii"><em>{localize('july', lang)}  22</em> <strong>Rapla <span>Kardirada</span></strong></li>
						<li className="-iv"><em>{localize('august', lang)}  12</em> <strong>Rapla <span>Kardirada</span></strong></li>
						<li className="-v"><em>{localize('september', lang)}  16</em> <strong>Porschering</strong></li>
					</ul>
				</figcaption>
			</figure>
		) : (<figure className="hero" data-path={props.path}>

			</figure>
		)}
		<main id="content">
			{props.children}
		</main>
		<Cookie lang={lang} />
    	<footer>
			<h2>Hobby Racing MTÜ</h2>
			<h3>reg.nr. <span>80577392</span></h3>
			<p>e-post: <a href="mailto:info@bestlap.ee">info@bestlap.ee</a></p>

			<h3>Jälgi meid ka</h3>
			<p><a href="https://www.facebook.com/bestlap.ee" target="_blank" rel="noreferrer">Facebookis</a> ja&nbsp;<a href="https://www.instagram.com/bestlap.ee" target="_blank" rel="noreferrer">Instagramis</a></p>
			<h3>Toetavad</h3>
			<Supporters />
		</footer>
    </div>);
};
/*
Layout.propTypes = {
    data: PropTypes.object.isRequired,
    pathContext: PropTypes.object.isRequired
};
*/

export default Layout;
