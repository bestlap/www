import axios from 'axios';

const api = process.env.API_BASE_URL;

const getToken = () => localStorage.getItem('token');
const setToken = (token) => localStorage.setItem('token', token);
const removeToken = () => localStorage.removeItem('token');

/* For React Native */
// const getTokenNative = async () => await AsyncStorage.getItem('token');
// const setTokenNative = () => AsyncStorage.setItem('token', response.data.token);

class Exception {
  constructor (exception) {
    this.status = 'rejected';
    this.reason = typeof exception === 'string' ? exception : exception?.response?.data?.reason || exception?.data?.reason || exception?.reason || exception?.message || exception;
  }
}

export const authorize = async () => {
  try {
    const token = getToken();

    if (!token) {
      return {
        status: 'rejected',
        reason: 'No means for authorization'
      };
    }
    
    const response = await axios.post(api, {}, {
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      }
    });

    return response.data;
  }

  catch (exception) {
    try {
      removeToken();
      
      throw new Exception(exception.response.data);
    }

    catch (reason) {
      throw reason;
    }
  }
};

export const login = async (email, password) => {
  try {
    const response = await axios.post(api, {}, {
      auth: {
        username: email,
        password: password,
      }
    });
    
    setToken(response.data.token);
  }
  
  catch (exception) {
    throw new Exception(exception);
  }
};

export const logout = async () => {
  try {
    removeToken();
  }

  catch (reason) {
    throw new Exception(reason);
  }
}

export const get = async that => {
  try {
    const response = await axios.post(`${api}${that}`, {}, {
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      }
    });

    return response.data;
  }

  catch (exception) {
    try {
      throw new Exception(exception.response.data.reason);
    }

    catch (reason) {
      throw new Exception(reason);
    }
  }
};

export const register = async (values, lang) => {
  try {
    const form = new FormData();

    for (const key in values) {
        form.append(key, values[key]);
    }

    const response = await axios.post(`${api}register`, form, {
      headers: {
        'Accept-Language': lang,
      },
    });

    return response.data;
  }

  catch (exception) {
    try {
      throw new Exception(exception.response.data.reason);
    }

    catch (reason) {
      throw new Exception(reason);
    }
  }
};

export const renew = async (values, lang) => {
  try {
    const form = new FormData();

    for (const key in values) {
        form.append(key, values[key]);
    }

    const response = await axios.post(`${api}renew`, form, {
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Accept-Language': lang,
      },
    });

    return response.data;
  }

  catch (exception) {
    try {
      throw new Exception(exception.response.data.reason);
    }

    catch (reason) {
      throw new Exception(reason);
    }
  }
};
