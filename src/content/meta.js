const languages = require('./languages');

module.exports = {
    title: 'Best Lap',
    siteUrl: 'https://www.bestlap.ee',
    menu: [
      {label: 'home', slug: '/'},
      {label: 'registration', slug: '/registration/'},
      {label: 'results', slug: '/results/'},
      {label: 'pilots', slug: '/pilots/'},
      {label: 'regulations', slug: '/regulations/'},
      {label: 'profile', slug: '/profile/'},
      {label: 'live', slug: '/live/'},
    ],  
    languages,
    contact: [
      {
        type: 'email',
        value: 'info@bestlap.ee',
        link: 'mailto:info@bestlap.ee'
      },
      {
        type: 'social',
        value: 'facebook',
        link: 'https://www.facebook.com/bestlap.ee'
      },
      {
        type: 'social',
        value: 'instagram',
        link: 'https://www.instagram.com/bestlap.ee'
      }
    ]
  };
  