import * as React from 'react';
import { FormikProps, FormikValues, connect, } from 'formik';
import debounce from 'lodash.debounce';
import isEqual from 'react-fast-compare';

export interface PersistProps {
  name: string;
  debounce?: number;
  isSessionStorage?: boolean;
  ignore?: string[];
}

class PersistImpl extends React.Component<
  PersistProps & { formik: FormikProps<any> },
  {}
> {
  static defaultProps = {
    debounce: 300,
  };

  saveForm = debounce((data: FormikProps<{}>) => {
    const filtered = this.props.ignore ? { ...data, values: { ...(this.props.ignore ? this.filter(data.values, this.props.ignore, data.initialValues) : data.values)}} : data;

    if (this.props.isSessionStorage) {
      window.sessionStorage.setItem(this.props.name, JSON.stringify(filtered));
    }
    
    else {
      window.localStorage.setItem(this.props.name, JSON.stringify(filtered));
    }
  }, this.props.debounce);

  componentDidUpdate(prevProps: PersistProps & { formik: FormikProps<any> }) {
    if (!isEqual(prevProps.formik, this.props.formik)) {
      this.saveForm(this.props.formik);
    }
  }

  componentDidMount() {
    const maybeState = this.props.isSessionStorage
      ? window.sessionStorage.getItem(this.props.name)
      : window.localStorage.getItem(this.props.name);
    
    if (maybeState && maybeState !== null) {
      this.props.formik.setFormikState(JSON.parse(maybeState));
    }
  }

  render() {
    return null;
  }

  private filter (current: FormikValues, ignore: string[], initial: FormikValues) {
    const result = {};
  
    for (const key in current) {
      result[key] = ignore.includes(key) ? initial[key] : current[key]
    }
  
    return result;
  }
  
}

export const Persist = connect<PersistProps, any>(PersistImpl);
