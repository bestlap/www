import * as React from "react"
import { Helmet } from "react-helmet";

import Supporters from "../components/supporters"
import FacebookFeed from "../components/facebook-feed";

import Layout from "../layouts"
import ReactMarkdown from 'react-markdown';
import { localize } from "../content";

const IndexPage = (props) => {
  return (
    <Layout {...{ ...props, index: true } }>
      <Helmet>
        <title>Best Lap</title>
      </Helmet>
      <h1>Best Lap <em>2023</em></h1>
      <section>
        <ReactMarkdown children={localize('intro', props.pageContext.langKey)} />
        {/*
                <div class="attention">⚠️⚠️⚠️</div>
        <h2>ÜRITUS JÄÄB ÄRA!</h2>
        <p>Peame teavitama, et 03.07 Best Lap teine etapp jääb samuti ära.</p>
        <p><b>Raja omanik teavitas täna, et vaatamata meile antud lubadusele ja kinnitustele ei olnud tehnilised probleemid kõrvaldatud. Omanik on probleemidest teavitanud ka omal leheküljel:<br />
        <a href="http://www.kardirada.ee/uudised/voistluspaus/">http://www.kardirada.ee/uudised/voistluspaus/</a></b></p>
        <ul>
          <li>03.07.2021 etapp jääb ära, me tagastame kõik makstud osalustasud (tänasest 7 päeva jooksul);</li>
          <li>teine etapp toimub 31.07 Raplas. Oleme üsna kindlad, et see etapp toimub. Raplas ei ole tehnilisi probleeme ja kõik üritused sel suvel on toimunud;</li>
          <li>14.08 etapp Tabasalus on küsimärgi all. Täpsemat infot jagame lähimal ajal.</li>
        </ul>
        <p>Loodame teie mõistvale suhtumisele ja kutsume teid 31.07 Raplasse.</p>
        <p><b>Registreerimine 31.07 etapile algab lähipäevil!</b></p>
        
        <div class="attention">⚠️⚠️⚠️</div>
        <h2>ÜRITUS JÄÄB ÄRA JA LÜKKUB EDASI!</h2>
        <p>Tabasalu Kardiarada teatas meid täna hommikul, et tehnilistel põhjustel ei ole 12.06.2021 kardirajal ürituse läbiviimine kuidagi võimalik.</p>

        <p><b>Vaatamata kõigile meie pingutustele ei olnud 12.06.2021 osas kokkuleppe saavutamine võimalik,<br />
        seega ÜRITUS JÄÄB ÄRA JA LÜKKUB EDASI!</b></p>

        <p>Uut kuupäeva anname teada lähipäevil.</p>

        <p>Loomulikult kõik, kes ei saa osaleda uuel kuupäeval saavad oma osalustasu tagasi!</p>

        <p>Vabandame ja täname toetuse eest!</p>

        <p>Best Lap meeskond</p>
        */}
        {/*
        <h2>Kallid vanad ja uued sõbrad!</h2>
        <p>Best Lap 2021 meeskond hea meelega esitab teile 2021 hooaja kalendri. Võtsime arvesse eelmise aasta kommentaare ja teeme sel aastal rohkem etappe.</p>
        <p>Registreerimine teisele etapile algab 12.05.2021, samal päeval ilmub info hooaja reeglite, tehniliste nõuete ja muu olulise kohta.</p>
        */}
        <Supporters />
        <h3>Jälgige kõiki uudiseid siin:</h3>
        <FacebookFeed />
      </section>
    </Layout>
  )
};

export default IndexPage
