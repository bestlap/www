import * as React from "react"
import { Helmet } from "react-helmet"
import { localize } from "../content"

import Layout from "../layouts"

export default function RegulationsPage (props) {
    const lang = props.pageContext.langKey;
    
    return (
        <Layout {...props}>
            <Helmet>
                <title>Регламент</title>
            </Helmet>
            <section id="regulations">
                <h2>{localize('regulations', lang)}</h2>
                <p>{localize('regulations-text', lang)}</p>
                <ul>
                    <li><a href="/docs/2023/RUS_Reglament_2023.pdf">Регламент</a></li>
                    <li><a href="/docs/2023/RUS_Bezopatnost_2023.pdf">Безопасность</a></li>
                    <li><a href="/docs/2023/RUS_Tehnicheski_Reglament_2023.pdf">Технические требования</a></li>
                    <li><a href="/docs/2023/RUS_Komandy_2023.pdf">Регламент командного зачета</a></li>
                </ul>

                <h3>Схема Porschering:</h3>
                <p>
                    <a href="/media/audru.jpg" className="-image"><img src="/media/audru.png" alt="Porschering" className="poster" /></a>
                </p>
                {/*
                <h3>Схема Rapla Kardirada:</h3>
                <p>
                    <a href="/media/rapla.jpg" className="-image"><img src="/media/rapla.png" alt="Rapla Kardirada" className="poster" /></a>
                </p>
               {/*
                <h3>Дополнительный регламент</h3>
                <ul>
                    
                    <li><a href="/docs/RUS_BestLap_Dop.Reglament_Audruring_01_May_2021_(30.03.21).pdf">Audruring,  01.05.2021</a></li>
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_Rapla_22_August_2021.pdf">Rapla, 22.08.2021</a> (на эстонском языке)</li>
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_PorscheRing_19_September_2021.pdf">Porsche Ring, 19.09.2021</a> (на эстонском языке)</li>

                    <li><a href="/docs/2022/RUS_BestLap_Dop.Reglament_07.05.2022_Porschering.pdf">Porsche Ring, 5.07.2022</a></li>
                    
                    <li><a href="/docs/2022/RUS_BestLap_Dop.Reglament_04.06.2022_Rapla.pdf">Rapla Kardirada, 4.06.2022</a></li>
                    
                    <li><a href="/docs/2022/RUS_BestLap_Dop.Reglament_09.07.2022_Porschering.pdf">Porsche Ring, 9.07.2022</a></li>
                    
                    <li><a href="/docs/2022/RUS_BestLap_Bulletaan_20.08.2022_Kulbilohu.pdf">Kulbilohu Krossirada, 20.08.22</a></li>
                    
                    <li><a href="/docs/2022/RUS_BestLap_Dop.Reglament_10.09.2022_Porschering.pdf">Porsche Ring, 10.09.2022</a></li>
                </ul>
                */}
                {/*
                <h3>RaceChrono</h3>
                <ul>
                    <li><a href="/media/track_kulbilohu_4.rcz">Kulbilohu Kardirada</a> track data for <a href="https://racechrono.com/">RaceChrono</a></li>
                </ul>
                
                <p>
                    <a href="/media/audru.jpg" className="-image"><img src="/media/audru.png" alt="Porsche Ring" className="poster" /></a>
                </p>
                */}
                
            </section>
        </Layout>
    )
};
