import * as React from "react"
import { navigate } from 'gatsby'

const url = '/regulations/';

class RedirectIndex extends React.PureComponent {
  constructor(args) {
    super(args);

    if (typeof window !== 'undefined') {
      navigate(url);
    }
  }

  render() {
    return (<div />);
  }
}

export default RedirectIndex;
