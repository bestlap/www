import * as React from "react"
import { Helmet } from "react-helmet"
import Layout from "../layouts"
import Registration from "../components/registration"

const RegistrationPage = (props) => {
  return (
    <Layout {...props}>
      <Helmet>
        <title>Registration</title>
      </Helmet>
      <section>
			  <h2>Registration</h2>
        <p>Porschering, 16.09.23.</p>
        <Registration lang={props.pageContext.langKey} />
      </section>
    </Layout>
  )
};

export default RegistrationPage
