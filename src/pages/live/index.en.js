import React, { useState } from "react"
import { Helmet } from "react-helmet"
import Time from "../../components/time"
import Modal from "../../components/modal"
import Laps from "../../components/time/laps"

import Layout from "../../layouts"

const LivePage = (props) => {
  const [modal, setModal] = useState(null);
  const click = laps => setModal(<Laps laps={laps} />);
  const close = () => setModal(null);

  return (
    <Layout {...props}>
        <Helmet>
            <title>Live</title>
            {/*
            <meta http-equiv="refresh" content="60"></meta>
            */}
        </Helmet>
        <section className="time">
          <Time {...props} live={true} onClick={click} />
        </section>
    </Layout>
  )
};

export default LivePage
