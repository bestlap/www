import * as React from "react"
import { Helmet } from "react-helmet";

import Supporters from "../components/supporters";
import FacebookFeed from "../components/facebook-feed";

import Layout from "../layouts"
import ReactMarkdown from 'react-markdown';
import { localize } from "../content";

const IndexPage = (props) => {
  return (
    <Layout {...{ ...props, index: true } }>
      <Helmet>
        <title>Best Lap</title>
      </Helmet>
      <h1>Best Lap <em>2023</em></h1>
      <section>
        <ReactMarkdown children={localize('intro', props.pageContext.langKey)} />
        {/*
                <div class="attention">⚠️⚠️⚠️</div>
        <h2>SECOND STAGE IS CANCELED!</h2>
        <p><b>The owners of Tabasalu track said that due to technical reasons, the race on 03.07.2021 is not possible:<br />
        <a href="http://www.kardirada.ee/uudised/voistluspaus/">http://www.kardirada.ee/uudised/voistluspaus/</a></b></p>

        <div class="attention">⚠️⚠️⚠️</div>
        <h2>SECOND STAGE IS CANCELED AND POSTPONED!</h2>
        <p>The owners of Tabasalu track said that due to technical reasons, the race on 12.06.2021 is not possible.</p>
        <p><b>Despite our best efforts, we were unable to agree and hold the race on June 12th.</b></p>
        <p><b>Therefore, we inform you that the SECOND STAGE IS CANCELED AND POSTPONED to another day.</b></p>
        <p>We will announce a new date in the coming days.</p>
        <p>Of course, everyone who cannot participate on another day will receive their participation fee back.</p>
        <p>We apologize and hope for your support!</p>
        <p>Best Lap Team</p>
        */}
        {/* 
        <h2>Dear old and new friends!</h2>
        <p>Best Lap 2021 team is delighted to present the 2021 racing schedule. We have listened to your feedback and tried to make more races during the new season.</p>
        <p>Registration for the 1st race will be opened 1st of April and on the same day we will publish the regulations and technical requirements for the season.</p>
        */}
        
        <Supporters />
        <h3>Check the news here:</h3>
        <FacebookFeed />
      </section>
    </Layout>
  )
};

export default IndexPage
