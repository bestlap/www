import * as React from "react"
import { Helmet } from "react-helmet"
import Registration from "../components/registration"

import Layout from "../layouts"

const RegistrationPage = (props) => {
  return (
    <Layout {...props}>
      <Helmet>
        <title>Registreerimine</title>
      </Helmet>
      <section>
			  <h2>Registreeri&shy;mine</h2>
        <p>Porschering, 16.09.23.</p>
        <Registration lang={props.pageContext.langKey} />
      </section>
    </Layout>
  )
};

export default RegistrationPage
