import * as React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../../layouts';
import Profile from '../../components/profile'

const ProfilePage = props => (
    <Layout {...props}>
        <Helmet>
            <title>Profile</title>
        </Helmet>
        <section id='profile'>
            <h2>Profile</h2>
            <Profile lang={props.pageContext.langKey} />
        </section>
    </Layout>
);

export default ProfilePage;
