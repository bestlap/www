import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

// console.log('results', Results)

import index from '../../../../../static/time/2022-09-10/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2022" date="09-10" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
