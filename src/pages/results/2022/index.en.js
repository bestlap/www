import * as React from "react"

import Layout from "../../../layouts"
import Results from '../../../layouts/results';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2022" {...props} />
  </Layout>
);

export default ResultsPage
