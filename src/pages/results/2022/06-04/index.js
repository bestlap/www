import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

import index from '../../../../../static/time/2022-06-04/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2022" date="06-04" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
