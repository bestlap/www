import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

// console.log('results', Results)

import index from '../../../../../static/time/2023-05-13/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2023" date="05-13" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
