import * as React from "react"

import Layout from "../../../layouts"
import Results from '../../../layouts/results';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2023" {...props} />
  </Layout>
);

export default ResultsPage
