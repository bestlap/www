import * as React from "react"
import { navigate } from 'gatsby'

const url = '/results/2023/06-10/';

class RedirectIndex extends React.PureComponent {
  constructor(args) {
    super(args);

    if (typeof window !== 'undefined') {
      navigate(url);
    }
  }

  render() {
    return (<div />);
  }
}

export default RedirectIndex;
