import React from 'react';
import Index from '.';

const ResultsPage = props => <Index {...props} />;

export default ResultsPage;
