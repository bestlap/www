import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

import index from '../../../../../static/time/2020-09-12/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2020" date="09-12" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
