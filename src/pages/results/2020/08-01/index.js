import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

import index from '../../../../../static/time/2020-08-01/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2020" date="08-01" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
