import React from 'react';

import Layout from '../../../../layouts';
import Results from '../../../../layouts/results';

import index from '../../../../../static/time/2021-05-01/index.json';

const ResultsPage = props => (
  <Layout {...props}>
    <Results year="2021" date="05-01" index={index} {...props} />
  </Layout>
);

export default ResultsPage;
