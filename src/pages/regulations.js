import * as React from "react"
import { Helmet } from "react-helmet"
import { localize } from "../content"

import Layout from "../layouts"

const RegulationsPage = (props) => {
    const lang = props.pageContext.langKey;
    
    return (
        <Layout {...props}>
            <Helmet>
                <title>Reglement</title>
            </Helmet>
            <section id="regulations">
                <h2>{localize('regulations', lang)}</h2>


                <p>Best Lap 2023. aasta sarjas on 5 etappi, mis toimuvad järgmise graafiku alusel:</p>

                <ol>
                    <li>13.05.2023 – Porsche Ring</li>
                    <li>10.06.2023 – Raassilla RX</li>
                    <li>22.07.2023 – Rapla Kardirada</li>
                    <li>12.08.2023 – Rapla Kardirada</li>
                    <li>16.09.2023 – Porsche Ring</li>
                </ol>

                <p>Registreerimine igale etapile avatakse kuu aega enne etapi päeva. Kogu informatsioon registreerimise kohta tuleb emailile (kui olete varem kasvõi korra osalenud) ja postitakse Best Lap Facebook lehele.</p>

                <h3>{localize('regulations-text', lang)}</h3>
                <ul>
                    <li><a href="/docs/2023/EST_BestLap_Ohutus_2023.pdf">Ohutusnõueded</a></li>
                    <li><a href="/docs/2023/EST_BestLap_Reglement_2023.pdf">Reglement</a></li>
                    <li><a href="/docs/2023/EST_BestLap_Tehniline_Reglement_2023.pdf">Tehniline reglement</a></li>
                    <li><a href="/docs/2023/EST_BestLap_Voistkondlik_Arvestus_2023.pdf">Võistkondliku arvestuse reglement</a></li>
                </ul>

                <h3>Iga etapi ajakava on järgmine:</h3>
                <dl>
                    <dt>Territooriumi avamine:</dt>
                    <dd>09:00</dd>
                    <dt>Tehniline kontroll ja registreerimine:</dt>
                    <dd>10:00—11:40</dd>
                    <dt>Trennisõidud:</dt>
                    <dd>10:00—12:00</dd>
                    <dt>Briifing:</dt>
                    <dd>12:15</dd>
                    <dt>Etappi algus:</dt>
                    <dd>12:30</dd>
                    <dt>Planeeritud ajasõitude lõpp:</dt>
                    <dd>~18:00</dd>
                    <dt>Tulemused ja autasustamine:</dt>
                    <dd>~18:15</dd>
                </dl>
                
                <h3>Porschering skeem:</h3>
                <p>
                    <a href="/media/audru.jpg" className="-image"><img src="/media/audru.png" alt="Porschering" className="poster" /></a>
                </p>
                {/*
                <h3>Rapla Kardirada skeem:</h3>
                <p>
                    <a href="/media/rapla.jpg" className="-image"><img src="/media/rapla.png" alt="Rapla Kardirada" className="poster" /></a>
                </p>
                
                <h3>Ürituse bülletäänid</h3>
                <ul>
                    
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_Audruring_01_May_2021_(30.03.21).pdf">Audruring, 01.05.2021</a></li>
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_Tabasalu_12_June_2021_(07.06.2021).pdf">Tabasalu, 12.06.2021</a></li>
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_Rapla_22_August_2021.pdf">Rapla, 22.08.2021</a></li>
                    <li><a href="/docs/EST_BestLap_Urituse_Bulletaan_PorscheRing_19_September_2021.pdf">Porsche Ring, 19.09.2021</a></li>
                    
                    <li><a href="/docs/2022/EST_BestLap_Bulletaan_07.05.2022 Porschering.pdf">Porsche Ring, 7.05.2022</a></li>
                    
                    <li><a href="/docs/2022/EST_BestLap_Bulletaan_04.06.2022_Rapla.pdf">Rapla Kardirada, 4.06.2022</a></li>
                    
                    <li><a href="/docs/2022/EST_BestLap_Bulletaan_09.07.2022_Porschering.pdf">Porsche Ring, 9.07.2022</a></li>
                    
                    <li><a href="/docs/2022/EST_BestLap_Bulletaan_20.08.2022_Kulbilohu.pdf">Kulbilohu Krossirada, 20.08.22</a></li>
                    
                    <li><a href="/docs/2022/EST_BestLap_Bulletaan_10.09.2022_Porschering.pdf">Porsche Ring, 10.09.2022</a></li>
                </ul>
                */}
                {/*
                <h3>RaceChrono</h3>
                <ul>
                    <li><a href="/media/track_kulbilohu_4.rcz">Kulbilohu Kardirada</a> track data for <a href="https://racechrono.com/">RaceChrono</a></li>
                </ul>
                
                <p>
                    <a href="/media/audru.jpg" className="-image"><img src="/media/audru.png" alt="Porsche Ring" className="poster" /></a>
                </p>
                */}
                
            </section>
        </Layout>
    )
};

export default RegulationsPage
