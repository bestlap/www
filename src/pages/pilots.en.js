import * as React from "react"
import { Helmet } from "react-helmet"
import Pilots from "../components/pilots"

import Layout from "../layouts"

const PilotsPage = (props) => {
  return (
    <Layout {...props}>
        <Helmet>
            <title>Participants</title>
        </Helmet>
        <section id="pilots">
            <h2>Participants</h2>
            <p>Porschering, 16.09.23.</p>
            <Pilots lang={props.pageContext.langKey} />
        </section>
    </Layout>
  )
};

export default PilotsPage
