import * as React from "react"
import { Helmet } from "react-helmet"

import Layout from "../layouts"

export default function NotFoundPage (props) {
  return (
    <Layout {...props}>
    <Helmet>
      <title>404</title>
    </Helmet>
    <section>
      <h2>404: Not Found</h2>
    </section>
  </Layout>
  )
}
