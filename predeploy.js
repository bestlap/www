import fs from 'fs';

import Index from './src/components/index.js';

const dates = [
/*
    '2020-06-13',
    '2020-08-01',
    '2020-09-12',

    '2021-05-01',
    '2021-07-31',
    '2021-08-22',
    '2021-09-19',

    '2022-05-07',
    '2022-06-04',
    '2022-07-09',
    '2022-08-20',
    '2022-09-10',

    '2023-05-13',

    '2023-06-10',

    '2023-07-22',
    
    '2023-08-12',
    */
    '2023-09-16',
];

const filenames = [
    'race',
    'racers',
    'cars',
    'classes',
    'laps',
    'rounds',
    'points',
    'messages',
    'numbers',
    'statuses',
];

try {
    await write (await read(dates, filenames));
} catch (e) {
    console.log(e);
}



async function read (dates, filenames) {
  const results = {};

  for (const date of dates) {
    results[date] = {};

    for (const filename of filenames) {
        try {
            results[date][filename] = JSON.parse(await fs.promises.readFile(`./static/time/${date}/${filename}.json`, "utf8" || undefined));
        } catch (e) {
            throw e;
        }
        
    }
  }
  
  return results;
}

async function write (everything) {
    for (const date in everything) {
        const filename = `./static/time/${date}/index.json`;
        const { race, racers, classes, cars, statuses, rounds, laps, points } = everything[date];
        const index = Index(race, racers, classes, cars, statuses, rounds, laps, points, true, 'bestlaps');
    
        await fs.promises.writeFile(filename, JSON.stringify(index, null, '    ') + '\n');

        console.log(filename, 'written'); 
    }

    return;
}
